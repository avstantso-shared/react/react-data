import React from 'react';

import { Reducer } from '@hooks/reducer';
import { SetStateAction } from '@root/types';

function example1() {
  type MyState = {
    x: string;
    y: number;
    z: { a: string };
    f: (n: number) => number;
  };

  const R = Reducer<MyState>().as((f) => ({
    MSG1: f('x'),
    MSG2: f('y'),
    MSGF: f.f('f'),
    MSGN: f('z')<boolean>((state, value) => {
      return { ...state, z: { a: value ? 'aa' : 'bb' } };
    }),
  }));

  //#region useStdReducer
  const [s1, d1] = R.useStdReducer();
  d1({ msg: 'MSG1', payload: (prev) => '1' });
  d1({ msg: 'MSG2', payload: 2 });
  d1({ msg: 'MSGN', payload: true });
  d1({ msg: 'MSGF', payload: () => (x) => 3 });
  d1.MSGF((prev) => (x) => prev(x + 1));
  //#endregion

  //#region useRefReducer
  const [s2, d2] = R.useRefReducer();
  d2({ msg: 'MSG1', payload: (prev) => '1' });
  d2({ msg: 'MSG2', payload: 2 });
  d2({ msg: 'MSGN', payload: true });
  d2({ msg: 'MSGF', payload: () => (x) => 3 });
  d2.MSGF((prev) => (x) => prev(x + 1));
  //#endregion

  //#region useRefReducer.Raw
  const [, setIncrement3] = React.useState(0);
  const ref3 = React.useRef<{ magic: typeof R.meta.ref }>();
  const initial3 = !ref3.current;
  if (initial3) ref3.current = {} as any;

  const [s3, d3] = R.useRefReducer.Raw('magic', ref3, initial3, () =>
    setIncrement3((prev) => prev + 1)
  );
  d3({ msg: 'MSG1', payload: (prev) => '1' });
  d3({ msg: 'MSG2', payload: 2 });
  d3({ msg: 'MSGN', payload: true });
  d3({ msg: 'MSGF', payload: () => (x) => 3 });
  d3.MSGF((prev) => (x) => prev(x + 1));
  //#endregion
}

export enum SortKind {
  none,
  asc,
  desc,
}

export namespace SortKind {
  export type Type = typeof SortKind;

  export namespace Key {
    export type List = ['none', 'asc', 'desc'];
  }

  export type Key = keyof Type;
  export type Value = Type[Key];
}

import { Enum, EnumWrap, Perform } from '@avstantso/node-or-browser-js--utils';
import type { Nested } from '../nested';

import * as _ from './kind-enum';

export namespace Sort {
  export namespace Kind {
    export type TypeMap = Enum.TypeMap.Make<
      _.SortKind.Type,
      _.SortKind.Key.List,
      {
        next: Sort.Kind;

        /**
         * @summary Sign of `Sort.Kind`
         * @param factor Factor. Default 1
         */
        sign(factor?: number): number;
      }
    >;

    export type Union = Enum.Value.Union<TypeMap>;
  }

  export type Kind = Enum.Value<Kind.TypeMap>;

  export namespace Column {
    export type Initializer = {
      (colIndex: number, sortKind?: Sort.Kind): Column;
      Dsc(colIndex: number): Column;
    };
  }

  export interface Column {
    colIndex: number;
    sortKind: Sort.Kind;
  }

  export namespace Comparision {
    export interface BaseType<T> {
      (fieldName: string): Comparision<T>;
      Dsc(fieldName: string): Comparision<T>;
    }

    export interface CoreType<T> {
      (): Comparision<T>;
      Dsc(): Comparision<T>;
    }

    export interface Custom<T> {
      (fieldName: string, comparer: Comparer<T>): Comparision<T>;
      (comparer: Comparer<T>): Comparision<T>;
    }

    export type Setup<T> = (comparisions: Comparision<T>) => unknown;
  }

  export interface Comparision<T> {
    //#region base types
    string: Comparision.BaseType<T>;
    number: Comparision.BaseType<T>;
    boolean: Comparision.BaseType<T>;
    Date: Comparision.BaseType<T>;
    json: Comparision.BaseType<T>;
    //#endregion

    //#region core types
    ID: Comparision.CoreType<T>;
    Named: Comparision.CoreType<T>;
    Described: Comparision.CoreType<T>;
    AuthoredLogin: Comparision.CoreType<T>;
    Activable: Comparision.CoreType<T>;
    Systemic: Comparision.CoreType<T>;
    Moduled: Comparision.CoreType<T>;
    PutDate: Comparision.CoreType<T>;
    UpdDate: Comparision.CoreType<T>;
    //#endregion

    //#region custom
    custom: Comparision.Custom<T>;
    //#endregion
  }

  export namespace Comparer {
    export type List<T> = Comparer<T>[];

    export type Complex<T> = (
      comparers?: Comparer.List<T>
    ) => (a: T, b: T) => number;
  }

  export type Comparer<T> = (a: T, b: T) => number;

  export namespace Methods {
    export type ByColIndex = (colIndex: number) => Column;
    export type Clear = () => void;
    export type Toggle = (colIndex: number, multiColumn?: boolean) => void;
    export type ToggleClick = (colIndex: number) => React.MouseEventHandler;
    export type Sort<TUnion> = <T extends Partial<TUnion>>(list: T[]) => T[];

    export type UseSorts = {
      <TItemOrUnion>(
        initialState: Perform<Column[] | Column>,
        comparisionSetup: Comparision.Setup<Partial<TItemOrUnion>>,
        nested?: Nested.Readonly<TItemOrUnion, TItemOrUnion>[]
      ): Sorts<TItemOrUnion>;
      <TItemOrUnion>(
        comparisionSetup: Comparision.Setup<Partial<TItemOrUnion>>,
        nested?: Nested.Readonly<TItemOrUnion, TItemOrUnion>[]
      ): Sorts<TItemOrUnion>;
    };
  }
}

export interface Sorts<TItemOrUnion = any> {
  columns: ReadonlyArray<Sort.Column>;
  byColIndex: Sort.Methods.ByColIndex;
  clear: Sort.Methods.Clear;
  toggle: Sort.Methods.Toggle;
  toggleClick: Sort.Methods.ToggleClick;
  sort: Sort.Methods.Sort<TItemOrUnion>;
}

function next(sortKind: Sort.Kind.Union): Sort.Kind {
  return Sort.Kind.fromValue(
    Sort.Kind.is.desc(sortKind) ? Sort.Kind.none : +sortKind + 1
  );
}

function sign(sortKind: Sort.Kind.Union, factor = 1): number {
  return (
    factor *
    Sort.Kind.fromValue(sortKind).switch<number>({
      none: 0,
      asc: 1,
      desc: -1,
    })
  );
}

class SortKindValue extends EnumWrap<Sort.Kind.TypeMap> {
  constructor(value: Sort.Kind.TypeMap['Value']) {
    super(value);
    Object.setPrototypeOf(this, SortKindValue.prototype);
  }

  public get next() {
    return next(this.value);
  }

  public sign(factor = 1) {
    return sign(this.value, factor);
  }
}

export const Column: Sort.Column.Initializer = (
  colIndex,
  sortKind = Sort.Kind.asc
) => ({ colIndex, sortKind });
Column.Dsc = (colIndex) => Column(colIndex, Sort.Kind.desc);

export const Sort = {
  Kind: Enum(_.SortKind, 'Sort.Kind', { next, sign })<Sort.Kind.TypeMap>(
    SortKindValue
  ),
  Column,
};

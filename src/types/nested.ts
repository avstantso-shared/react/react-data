export namespace Nested {
  export type Getter<TParent = any, TNested = any> = (
    parent: TParent
  ) => TNested[];

  export type Setter<TParent = any, TNested = any> = (
    parent: TParent,
    nested: TNested[]
  ) => TParent;

  export type Readonly<TParent = any, TNested = any> =
    | string
    | Getter<TParent, TNested>;

  export namespace Writable {
    export type Item<TParent = any, TNested = any> = {
      getter: Getter<TParent, TNested>;
      setter: Setter<TParent, TNested>;
    };
  }

  export type Writable<TParent = any, TNested = any> =
    | string
    | Writable.Item<TParent, TNested>;
}

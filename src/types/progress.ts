export interface Progress {
  value: number;
  reset(): void;
  handler(progressEvent: any): void;
}

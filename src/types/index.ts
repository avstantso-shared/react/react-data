import { GenericFunctionComponent } from './generic-fc';

export {
  GenericFunctionComponent,
  GenericFunctionComponent as GenericFC,
  GenericFunctionComponent as GFC,
};

export * from './alternate-jsx';
export * from './dataCollection';
export * from './dblclick';
export * from './debugSentry';
export * from './docBody';
export * from './filter';
export * from './folding';
export * from './gender';
export * from './idsSetContainer';
export * from './link';
export * from './locker';
export * from './misc';
export * from './nested';
export * from './progress';
export * from './react';
export * from './selection';
export * from './sort';
export * from './state-rec';
export * from './urlActions';

import type { Componented } from '../misc';

export namespace SubDialogs {
  export interface Props {
    dialogs?:
      | ReadonlyArray<Componented>
      | Readonly<Componented>
      | NodeJS.Dict<Componented>;
  }
}

import type { Base } from './base';

export interface Message extends Base {
  execute(
    text: string | JSX.Element,
    title?: string | JSX.Element
  ): Promise<boolean>;
}

import Bluebird from 'bluebird';

import type { Perform } from '@avstantso/node-or-browser-js--utils';
import type { Closable as MiscClosable, DebugLabel } from '../misc';
import type { Base } from './base';

export namespace Entity {
  export interface Result<TEntity> {
    isClosed: boolean;
    isCanceled: boolean;
    entity?: TEntity;
    redirectUrl?: string;
  }

  export namespace Result {
    export interface Closable<TEntity> extends Result<TEntity>, MiscClosable {}
  }

  export type Redirect = (url: string) => void;

  export namespace Handler {
    export type Raw<TEntity> = (
      result: Result.Closable<TEntity>
    ) => Promise<void>;
  }

  export interface Handler<TEntity> {
    cancel: () => Promise<void>;
    apply: (entity: TEntity) => Promise<void>;
    accept: (entity: TEntity) => Promise<void>;
    redirect?: Redirect;
    raw: (result: Entity.Result.Closable<TEntity>) => Promise<void>;
  }

  export type Options = DebugLabel.Option;

  export namespace Execute {
    export interface Options<
      TEntity extends Perform.Able,
      TParams extends Perform.Able = never
    > extends DebugLabel.Option {
      entity?: Perform<TEntity>;
      params?: Perform<TParams>;
    }

    export type Promise<
      TEntity extends Perform.Able,
      TParams extends Perform.Able = never
    > = (options?: Options<TEntity, TParams>) => Bluebird<Result<TEntity>>;
  }

  export type Execute<
    TEntity extends Perform.Able,
    TParams extends Perform.Able = never
  > = (
    callBack: Handler.Raw<TEntity>,
    options?: Execute.Options<TEntity, TParams>
  ) => void;

  export namespace ComponentBuilder {
    export type Props<TEntity, TParams = never> = {
      handler: Handler<TEntity>;
      entity: TEntity;
      params?: TParams;
    };
  }

  export type ComponentBuilder<TEntity, TParams = never> = (
    props: ComponentBuilder.Props<TEntity, TParams>
  ) => JSX.Element;

  export interface State<
    TEntity extends Perform.Able,
    TParams extends Perform.Able = never
  > {
    handler: Entity.Handler.Raw<TEntity>;
    entity: Perform<TEntity>;
    params: Perform<TParams>;
  }

  /**
   * @summary For use in dependent components
   */
  export interface Props<TEntity extends Perform.Able> {
    entity: Perform<TEntity>;
    handler: Handler<TEntity>;
  }
}

export interface Entity<
  TEntity extends Perform.Able,
  TParams extends Perform.Able = never
> extends Base {
  execute: Entity.Execute<TEntity, TParams>;
  promise: Entity.Execute.Promise<TEntity, TParams>;
}

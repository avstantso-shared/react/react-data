import type { Componented, Closable, DebugLabel } from '../misc';

export interface Base extends Componented, Closable, DebugLabel {
  readonly isShowed: boolean;
}

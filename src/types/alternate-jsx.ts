import React from 'react';

import { Generics, JS } from '@avstantso/node-or-browser-js--utils';

namespace Methods {
  export type Extract = {
    <
      TStrictString extends boolean = undefined,
      TAltJSX extends AlternateJSX.Union = AlternateJSX.Union
    >(
      altJSX: TAltJSX,
      strictString?: TStrictString
    ): AlternateJSX.Parse<TStrictString, TAltJSX>;

    Str<TAltJSX extends AlternateJSX.Union = AlternateJSX.Union>(
      altJSX: TAltJSX
    ): string;
  };

  export type Getter = <
    TParams extends AlternateJSX.Getter.Params,
    TAltJSX extends AlternateJSX.Union = AlternateJSX.Union
  >(
    raw: AlternateJSX.Getter.Raw<TParams, TAltJSX>
  ) => AlternateJSX.Getter<TParams, TAltJSX>;
}

type Methods = {
  (jsx: React.ReactNode, alt: AlternateJSX.Alt): AlternateJSX;

  Is(candidate: unknown): candidate is AlternateJSX;
  Extract: Methods.Extract;

  Getter: Methods.Getter;
};

export namespace AlternateJSX {
  export type Alt = Exclude<React.ReactNode, object>;

  export type Union = React.ReactNode | AlternateJSX;

  export type Parse<
    TStrictString extends boolean,
    TAltJSX extends Union = Union
  > = TStrictString extends true
    ? string
    : TAltJSX extends AlternateJSX
    ? AlternateJSX['jsx']
    : TAltJSX;

  export namespace Getter {
    export namespace Params {
      export type Merge<
        TParams extends Params,
        TStrictString extends boolean = boolean,
        R extends [...TParams, TStrictString?] = [...TParams, TStrictString?]
      > = R;
    }

    export type Params = unknown[];

    export type Raw<TParams extends Params, TAltJSX extends Union = Union> = (
      ...params: Params.Merge<TParams>
    ) => Parse<false, TAltJSX>;

    export namespace Check {
      export type Ex<
        TParams extends AlternateJSX.Getter.Params,
        TAltJSX extends Union,
        TGetter extends AlternateJSX.Getter<TParams, TAltJSX>
      > = TGetter;
    }

    export type Check<
      TParams extends AlternateJSX.Getter.Params,
      TGetter extends AlternateJSX.Getter<TParams, Union>
    > = Check.Ex<TParams, Union, TGetter>;
  }

  export type Getter<
    TParams extends AlternateJSX.Getter.Params,
    TAltJSX extends Union = Union
  > = <TStrictString extends boolean = undefined>(
    ...params: AlternateJSX.Getter.Params.Merge<TParams, TStrictString>
  ) => Parse<TStrictString, TAltJSX>;
}

export type AlternateJSX = {
  jsx: React.ReactNode;
  alt: AlternateJSX.Alt;
};

export const AlternateJSX: Methods = (jsx, alt) => ({ jsx, alt });

function isAlternateJSX(candidate: unknown): candidate is AlternateJSX {
  return (
    null !== candidate &&
    JS.is.object(candidate) &&
    'jsx' in candidate &&
    'alt' in candidate
  );
}

const Extract: Methods.Extract = <
  TStrictString extends boolean = undefined,
  TAltJSX extends AlternateJSX.Union = AlternateJSX.Union
>(
  altJSX: TAltJSX,
  strictString?: TStrictString
): AlternateJSX.Parse<TStrictString, TAltJSX> =>
  Generics.Cast.To(
    isAlternateJSX(altJSX)
      ? !strictString
        ? altJSX.jsx
        : altJSX.alt
      : !strictString
      ? altJSX
      : false === altJSX
      ? ''
      : `${altJSX}`
  );

Extract.Str = (reactNode) => Extract(reactNode, true);

AlternateJSX.Is = isAlternateJSX;
AlternateJSX.Extract = Extract;
AlternateJSX.Getter = Generics.Cast;

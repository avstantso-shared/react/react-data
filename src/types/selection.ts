import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { IdsSet } from './idsSetContainer';

export namespace Selection {
  export interface Raw extends IdsSet.Container {
    toggleAll(isSelected: boolean): void;
    toggleFor(isSelected: boolean, ...items: Model.IDed[] | string[]): void;
  }

  export interface Controller<T extends Model.IDed> {
    getSelected?(entity: T): boolean;
    toggleSelection?(entity: T, selected: boolean): void;
  }
}

export interface Selection<T extends Model.IDed> extends Selection.Raw {
  controller(): Selection.Controller<T>;
  items(): T[];
}

export interface SelectionsSet {
  readonly isAllSelected: boolean;
  readonly selectionSize: number;
  toggleSelectAll(e?: any): any;
  clear(): void;
}

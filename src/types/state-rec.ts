import React from 'react';
import { TS } from '@avstantso/node-or-browser-js--utils';

type _StateRec<
  TName extends string,
  TState,
  TOptions extends StateRec.Options = undefined,
  TWithoutGet extends boolean = false,
  Options extends StateRec.Options.Apply<TOptions> = StateRec.Options.Apply<TOptions>,
  Get = {
    [K in TName]: TState;
  },
  Set = {
    [K in `set${Capitalize<TName>}${string extends Options['SetterPostfix']
      ? ''
      : Options['SetterPostfix']}`]: React.Dispatch<
      React.SetStateAction<TState>
    >;
  },
  R = (TWithoutGet extends true
    ? {}
    : TS.If<Options['GetIsOpt'], Partial<Get>, Get>) &
    TS.If<Options['SetIsOpt'], Partial<Set>, Set>
> = {
  [K in keyof R]: R[K];
};

/**
 * @summary Create record with properties for store `React.useState` result
 * @param TName State name
 * @param TState State type
 * @param TOptionsOrOptionalOrPostfix `true/false` — is optional, `string` — postfix, `object` — options
 */
export type StateRec<
  TName extends string,
  TState,
  TOptionsOrOptionalOrPostfix extends
    | StateRec.Options
    | boolean
    | string = undefined
> = _StateRec<
  TName,
  TState,
  StateRec.Options.FromUnion<TOptionsOrOptionalOrPostfix>
>;

export namespace StateRec {
  /**
   * Options for `StateRec`
   */
  export type Options = Options.Structure['TypeMap'];

  export namespace Options {
    export type Structure = TS.Options<
      {
        SetterPostfix: string;
        GetIsOpt: boolean;
        SetIsOpt: boolean;
      },
      { SetterPostfix: '' }
    >;

    export type Apply<TOptions extends Options> = TS.Options.Apply<
      Options.Structure,
      TOptions
    >;

    export type FromUnion<
      TOptionsOrOptionalOrPostfix extends StateRec.Options | boolean | string
    > = TS.Options.Check<
      Options.Structure,
      TOptionsOrOptionalOrPostfix extends undefined
        ? undefined
        : TOptionsOrOptionalOrPostfix extends boolean
        ? TOptionsOrOptionalOrPostfix extends true
          ? { GetIsOpt: true; SetIsOpt: true }
          : undefined
        : TOptionsOrOptionalOrPostfix extends string
        ? { SetterPostfix: Extract<TOptionsOrOptionalOrPostfix, string> }
        : Extract<TOptionsOrOptionalOrPostfix, object>
    >;
  }
}

/**
 * @summary Create records with properties for store `React.useState` result
 * @param T Object template
 * @param TOptionsOrOptionalOrPostfix `true/false` — is optional, `string` — postfix, `object` — options
 */
export type StateRecs<
  T extends object,
  TOptionsOrOptionalOrPostfix extends
    | StateRec.Options
    | boolean
    | string = undefined,
  Raw = TS.IfStructure<T> extends true
    ? {
        [K in keyof T]: _StateRec<
          Extract<K, string>,
          T[K],
          StateRec.Options.FromUnion<TOptionsOrOptionalOrPostfix>
        >;
      }
    : never,
  R = TS.Flat<Raw>
> = { [K in keyof R]: R[K] };

export namespace StateRecs {
  /**
   * @summary Create setters for store `React.useState` result
   * @param T Object template
   * @param TOptionsOrOptionalOrPostfix `true/false` — is optional, `string` — postfix, `object` — options
   */
  export type Setters<
    T extends object,
    TOptionsOrOptionalOrPostfix extends
      | StateRec.Options
      | boolean
      | string = undefined,
    R = TS.IfStructure<T> extends true
      ? {
          [K in keyof T]: _StateRec<
            Extract<K, string>,
            T[K],
            StateRec.Options.FromUnion<TOptionsOrOptionalOrPostfix>,
            true
          >;
        }
      : never
  > = TS.Flat<R>;
}

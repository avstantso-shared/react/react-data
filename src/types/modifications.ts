import type { StateRec } from './state-rec';

export namespace Modifications {
  export namespace Details {
    export type Item<T = any> = Readonly<{
      current: Readonly<T>;
      unmodified: Readonly<T>;
      isMatch: boolean;
    }>;
  }

  export type Details<TEntity = any> = {
    [K in keyof TEntity]: Details.Item<TEntity[K]>;
  };

  export type State = Pick<Modifications.Raw, 'modified' | 'details'>;

  export type Check<T = any> = (current: T, unmodified: T) => boolean;
  export type Checks<TEntity = any> = {
    [K in keyof TEntity]?: Check<TEntity[K]>;
  };

  export namespace Callback {
    export type Wrap = <TParams extends any[] = never, TResult = unknown>(
      callback: Modifications.Callback<TParams, TResult>,
      ...params: TParams
    ) => TResult | void;
  }

  export type Callback<TParams extends any[] = never, TResult = unknown> = (
    ...params: TParams
  ) => TResult;

  export namespace Behavior {
    export type Options<TEntity = any> = Readonly<{
      extCheck?(entity: TEntity): boolean;
      silent?: boolean;
      customChecks?: Modifications.Checks<TEntity>;
    }>;
  }

  export type Options<TEntity = any> = Behavior.Options<TEntity> &
    Readonly<{
      defaultState?: boolean;
      debug?: boolean;
    }>;

  export type Raw<TEntity = any> = StateRec<'modified', boolean> & {
    details: Modifications.Details;
    prompt: Readonly<{ when: boolean; message: string }>;
    update(entity: TEntity): void;
    save: Modifications.Callback.Wrap;
    confirm: Modifications.Callback.Wrap;
  };
}

export type Modifications<TEntity = any> = Readonly<Modifications.Raw<TEntity>>;

import { TS } from '@avstantso/node-or-browser-js--utils';

const linkRel = TS.Type.Definer().Arr('opener', 'referrer', 'nofollow');

export namespace Link {
  export namespace Rel {
    export type Key = TS.Array.KeyFrom<typeof linkRel>;
  }

  export type Rel = Partial<Record<Rel.Key, boolean>>;
}

export const Link = {
  Rel: {
    toRel: ({ opener, referrer, nofollow }: Link.Rel = {}) => ({
      rel: [
        ...(opener ? [] : ['noopener']),
        ...(referrer ? [] : ['noreferrer']),
        ...(nofollow ? ['nofollow'] : []),
      ].join(' '),
    }),
  },
};

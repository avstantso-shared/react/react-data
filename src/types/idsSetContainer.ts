import type { Model } from '@avstantso/node-or-browser-js--model-core';

export namespace IdsSet {
  export interface Container {
    readonly data: IdsSet;
    setData: React.Dispatch<React.SetStateAction<IdsSet>>;
    clone(): IdsSet;
    clear(): void;
  }

  export interface Management {
    empty(): IdsSet;
    fromDataList(list: ReadonlyArray<Model.IDed>): IdsSet;
  }
}

export type IdsSet = Set<Model.ID>;

import React from 'react';
import * as Utils from '@avstantso/node-or-browser-js--utils';

export interface Componented {
  Component: React.FC<{}>;
}

function isComponented(candidate: unknown): candidate is Componented {
  return (
    null !== candidate &&
    Utils.JS.is.object(candidate) &&
    'Component' in candidate
  );
}

export const Componented = { is: isComponented };

export interface Closable {
  close(): void;
}

export interface EntityFormConverter<T> {
  to(entity: T): T;
  from(entity: T): T;
}

export type Caption = string | JSX.Element;
export type PerformCaptions = Utils.Perform<ReadonlyArray<Caption>>;

export type ChildrenProps = {
  children?: React.ReactNode | React.ReactNode[];
};

export namespace ChildrenProps {
  export interface Perform {
    children?: Utils.Perform<React.ReactNode | React.ReactNode[]>;
  }
}

export namespace DebugLabel {
  export interface Option {
    readonly label?: Utils.Perform<string>;
  }
}

export interface DebugLabel {
  readonly label?: string;
}

export const DebugLabel = {
  Prefix: (named: string | Function, options?: DebugLabel.Option) => {
    const label = options?.label && Utils.Perform(options.label);
    return `${Utils.JS.is.string(named) ? named : named.name}${
      label ? `(${label})` : ''
    }`;
  },
};

export namespace Indexed {
  export interface Ref {
    readonly indexRef: React.MutableRefObject<number>;
  }
}

export interface Indexed {
  readonly index: number;
}

export namespace InnerRef {
  export type Provider<T extends HTMLElement = HTMLElement> = {
    innerRef?: React.LegacyRef<T>;
  };
}

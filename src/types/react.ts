import type { useEffect } from 'react';

import { JS, TS } from '@avstantso/node-or-browser-js--utils';

export type EffectCallback = Parameters<typeof useEffect>[0];

export type Destructor = Extract<ReturnType<EffectCallback>, Function>;

export type SetStateAction<S> = S extends Function
  ? Extract<React.SetStateAction<S>, TS.Type.Func.Nested>
  : S extends boolean
  ? React.SetStateAction<boolean>
  : React.SetStateAction<S>;

export const SetStateAction = {
  resolve: <S>(action: SetStateAction<S>, prev?: S): S =>
    JS.is.function(action) ? action(prev) : action,
};

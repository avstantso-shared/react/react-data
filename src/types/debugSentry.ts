import React from 'react';
import type { DebugLabel } from './misc';

export namespace DebugSentry {
  export interface Options extends DebugLabel.Option {}

  export type Snapshot = Readonly<{
    frame: number;
    deps: React.DependencyList;
  }>;

  export interface DependencyDiff {
    index: number;
    old: any;
    new: any;
  }
}

export type DebugSentry = Readonly<
  DebugLabel & {
    frame: number;
    lastSnapshot?: DebugSentry.Snapshot;
    immediateUpdates: number;
    update(deps: React.DependencyList, warnThreshold?: number): void;
  }
>;

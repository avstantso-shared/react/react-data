/* eslint no-bitwise: "off" */
import React from 'react';

import type { Perform } from '@avstantso/node-or-browser-js--utils';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import { DebugLabel } from './misc';

export namespace DataCollection {
  export type Options<T extends Model.Select> = Readonly<
    DebugLabel.Option & {
      original?: Perform<ReadonlyArray<T>>;
      externalDeps?: React.DependencyList;
      unique?: Perform<boolean>;
      desc?: Perform<boolean>;
    }
  >;

  export type State<T extends Model.Select> = {
    list: ReadonlyArray<T>;
    total: number;
  };

  export type Readable<T extends Model.Select> = DebugLabel &
    Readonly<State<T>> & {
      readonly version: number;
      copy(): T[];
    };

  export type Writable<T extends Model.Select> = {
    setter: React.Dispatch<React.SetStateAction<ReadonlyArray<T>>>;
    setTotal: React.Dispatch<React.SetStateAction<number>>;

    clear(): void;
    replace(...rest: T[]): void;
    add(...rest: T[]): void;
    addOrSet(...rest: T[]): void;
    delete(...rest: (T | Model.ID)[]): void;
  };

  export namespace Trigger {
    export enum Action {
      Insert = 1 << 0,
      Update = 1 << 1,
      Delete = 1 << 2,

      Any = Insert | Update | Delete,

      InsertUpdate = Insert | Update,
      InsertDelete = Insert | Delete,
      UpdateDelete = Update | Delete,
    }

    export type Options<T extends Model.Select> = DebugLabel.Option & {
      /**
       * @summary Trigger calls only for specified actions
       */
      actions?: Action;

      /**
       * @summary Trigger calls are disabled
       */
      disabled?: boolean;

      /**
       * @summary Trigger calls for `Update` only if all specified fields changed
       */
      changesMatch?: (keyof T)[];
    };

    export namespace Callback {
      export type Props<T extends Model.Select> = {
        list: ReadonlyArray<T>;
        oldData: T;
        newData: T;
        action: Action;
      };
    }

    export type Callback<T extends Model.Select> = (
      props: Callback.Props<T>
    ) => void;

    export type Use = <T extends Model.Select>(
      collection: DataCollection<T>,
      callback: Callback<T>,
      options?: Options<T>
    ) => void;
  }

  export type Trigger<T extends Model.Select> = DebugLabel & {
    callback: Trigger.Callback<T>;
  } & Trigger.Options<T>;

  export type TriggersList<T extends Model.Select> = React.MutableRefObject<
    Trigger<T>
  >[];

  export type Use = <T extends Model.Select>(
    options?: Options<T>
  ) => DataCollection<T>;
}

export type DataCollection<T extends Model.Select> =
  DataCollection.Readable<T> & DataCollection.Writable<T> & ReadonlyArray<T>;

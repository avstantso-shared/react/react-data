import React from 'react';

import type { Generic } from '@avstantso/node-or-browser-js--utils';

import type { InnerRef } from './misc';

export namespace GenericFunctionComponent {
  export type Static<P = {}> = {
    [K in keyof React.FunctionComponent<P>]?: React.FunctionComponent<P>[K];
  };

  export type Props<P = {}> = Parameters<React.FunctionComponent<P>>[0];
  export type Context<P = {}> = Parameters<React.FunctionComponent<P>>[1];
  export type Result<P = {}> = ReturnType<React.FunctionComponent<P>>;

  export type TypeMap<
    P = {},
    TProps extends Props<P> = Props<P>,
    TContext extends Context<P> = Context<P>,
    TResult extends Result<P> = Result<P>
  > = {
    Props: TProps;
    Context: TContext;
    Result: TResult;
  };

  export type AsElement<
    T extends React.ElementType,
    D extends React.ElementType = undefined
  > = (D extends undefined ? { as: T } : { as?: T }) &
    React.ComponentPropsWithoutRef<T> &
    InnerRef.Provider<Generic>;

  export namespace AsElement {
    export type TypeMap<
      T extends React.ElementType,
      D extends React.ElementType = undefined,
      P extends AsElement<T, D> = AsElement<T, D>
    > = GenericFunctionComponent.TypeMap<P>;

    export type FC<B = {}> = Static<B> & {
      <T extends React.ElementType, D extends React.ElementType = undefined>(
        props: TypeMap<T, D>['Props'] & B,
        context: TypeMap<T, D>['Context']
      ): TypeMap<T, D>['Result'];
    };

    export type Fragment<T extends React.ElementType> = AsElement<
      T,
      typeof React.Fragment
    >;
    export namespace Fragment {
      export type TypeMap<T extends React.ElementType> = AsElement.TypeMap<
        T,
        typeof React.Fragment
      >;

      export type FC<B = {}> = Static<B> & {
        <T extends React.ElementType>(
          props: TypeMap<T>['Props'] & B,
          context: TypeMap<T>['Context']
        ): TypeMap<T>['Result'];
      };
    }

    export type Div<T extends React.ElementType> = AsElement<T, 'div'>;
    export namespace Div {
      export type TypeMap<T extends React.ElementType> = AsElement.TypeMap<
        T,
        'div'
      >;

      export type FC<B = {}> = Static<B> & {
        <T extends React.ElementType>(
          props: TypeMap<T>['Props'] & B,
          context: TypeMap<T>['Context']
        ): TypeMap<T>['Result'];
      };
    }

    export type Span<T extends React.ElementType> = AsElement<T, 'span'>;
    export namespace Span {
      export type TypeMap<T extends React.ElementType> = AsElement.TypeMap<
        T,
        'span'
      >;

      export type FC<B = {}> = Static<B> & {
        <T extends React.ElementType>(
          props: TypeMap<T>['Props'] & B,
          context: TypeMap<T>['Context']
        ): TypeMap<T>['Result'];
      };
    }
  }
}

export type GenericFunctionComponent<B = {}> = {
  <P extends B = B>(
    props: GenericFunctionComponent.Props<P>,
    context?: GenericFunctionComponent.Context<P>
  ): GenericFunctionComponent.Result<P>;
} & GenericFunctionComponent.Static<B>;

function AsElement<D extends React.ElementType = undefined>(
  def?: D,
  defClassName?: string
) {
  return <
    T extends React.ElementType,
    TProps extends GenericFunctionComponent.AsElement<T, D>
  >(
    props: TProps
  ): JSX.Element => {
    const {
      as: As = def,
      className,
      children,
      innerRef,
      ...rest
    } = props as Generic;

    const classes =
      As === React.Fragment ? [] : [defClassName, className].pack();

    return React.createElement(
      As,
      {
        ...rest,
        ...(classes.length
          ? { className: classes.join(' '), ref: innerRef }
          : {}),
      },
      children
    );
  };
}

AsElement.Fragment = AsElement.bind(null, React.Fragment);
AsElement.Div = AsElement.bind(null, 'div');
AsElement.Span = AsElement.bind(null, 'span');

export const GenericFunctionComponent = { AsElement };

export enum UrlActions {
  Root = 1,
  Trash = 2,
  View = 4,
  New = 8,
  Edit = 16,
  Delete = 32,
  Restore = 64,
}

export namespace UrlActions {
  export type Type = typeof UrlActions;

  export namespace Key {
    export type List = [
      'Root',
      'Trash',
      'View',
      'New',
      'Edit',
      'Delete',
      'Restore'
    ];

    export type Map = {
      1: 'Root';
      2: 'Trash';
      4: 'View';
      8: 'New';
      16: 'Edit';
      32: 'Delete';
      64: 'Restore';
    };
  }

  export type Key = keyof Type;
  export type Value = Type[Key];
}

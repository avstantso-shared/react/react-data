import { validate as isValidUUID } from 'uuid';

import {
  Enum,
  EnumWrap,
  JS,
  Perform,
} from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import * as _ from './enum';

export namespace UrlActions {
  export type TypeMap = Enum.TypeMap.Make<
    _.UrlActions.Type,
    _.UrlActions.Key.List,
    {
      KeyMap: _.UrlActions.Key.Map;
      toPath(): string;
      toPathPostfix(pathname?: string, idOrEd?: Model.ID | Model.IDed): string;
      idFromPathParts: Methods.InClass.IdFromPathParts;
      isRead: boolean;
    }
  >;

  export type Union = Enum.Value.Union<TypeMap>;

  /**
   * @summary Type for derived wrapper of toPathPostfix()
   * @param urlAction Action, default UrlActions.Root
   * @param id Entity id
   */
  export type PathPostfixFunc = (
    urlAction?: UrlActions.Union,
    id?: Model.ID
  ) => string;

  export type PathParts = [string, string?, string?];

  export type RouteItem = {
    action: UrlActions;
    id?: Model.ID;
  };

  export type RouteItems = Map<string, RouteItem>;

  export namespace Methods {
    export type FromPathParts = {
      (...parts: string[]): UrlActions;
      (parts: string[]): UrlActions;
      (parts: PathParts): UrlActions;
    };

    export type IdFromPathParts = {
      (action: UrlActions.Union, ...parts: string[]): Model.ID;
      (action: UrlActions.Union, parts: string[]): Model.ID;
      (action: UrlActions.Union, parts: PathParts): Model.ID;
    };

    export namespace ReplaceInPath {
      export type To =
        | UrlActions.Union
        | ((from?: UrlActions.Union, id?: Model.ID) => UrlActions.Union);
    }

    export type ReplaceInPath = {
      (pathname: string, pathPrefix: string, to: ReplaceInPath.To): string;
      (pathname: string, to: ReplaceInPath.To): string;
    };

    export namespace InClass {
      export type IdFromPathParts = {
        (...parts: string[]): Model.ID;
        (parts: string[]): Model.ID;
        (parts: PathParts): Model.ID;
      };
    }
  }

  /**
   * @summary Every `UrlActions` in this set has `id` url path part
   */
  export type WithId = 'View' | 'Edit' | 'Delete' | 'Restore';

  /**
   * @summary Every `UrlActions` in this set has named url path part
   */
  export type WithPath = 'Trash' | 'New' | 'Edit' | 'Delete' | 'Restore';
}

export type UrlActions = Enum.Value<UrlActions.TypeMap>;

class UrlActionsValue extends EnumWrap<UrlActions.TypeMap> {
  constructor(value: UrlActions.TypeMap['Value']) {
    super(value);
    Object.setPrototypeOf(this, UrlActionsValue.prototype);
  }

  public toPath() {
    return toPath(this.value);
  }

  public toPathPostfix(pathname?: string, idOrEd?: Model.ID | Model.IDed) {
    return toPathPostfix(this.value, pathname, idOrEd);
  }

  public get idFromPathParts(): UrlActions.Methods.InClass.IdFromPathParts {
    return (...params: any[]) => idFromPathParts(this.value, ...params);
  }

  public get isRead() {
    return isRead(this.value);
  }
}

function toPath(action: UrlActions.Union): string {
  return UrlActions.is(
    action,
    UrlActions.Trash,
    UrlActions.New,
    UrlActions.Edit,
    UrlActions.Delete,
    UrlActions.Restore
  )
    ? UrlActions.toName(action).toLocaleLowerCase()
    : '';
}

const PathParts = {
  /**
   * @summary Maximum path parts for analyze
   */
  max: 3,
};

function fromPathPartsParseParams(params: any[]): UrlActions.PathParts {
  return (Array.isArray(params[0]) ? params[0] : params).slice(
    0,
    PathParts.max
  ) as UrlActions.PathParts;
}

const fromPathParts: UrlActions.Methods.FromPathParts = (
  ...params: any[]
): UrlActions => {
  const parts = fromPathPartsParseParams(params);

  for (let i = 0; i < parts.length; i++) if (null === parts[i]) return null;

  if (!parts[0]) return UrlActions.Root;

  if (toPath(UrlActions.Trash) === parts[0]) {
    const sub = fromPathParts(parts[1], parts[2]);
    if (
      UrlActions.is(sub, UrlActions.View, UrlActions.Delete, UrlActions.Restore)
    )
      return UrlActions.fromValue(+UrlActions.Trash | +sub);
    else return UrlActions.Trash;
  }

  return toPath(UrlActions.New) === parts[0]
    ? UrlActions.New
    : !parts[1]
    ? UrlActions.View
    : UrlActions.fromString.uncapitalized(parts[1], UrlActions.View);
};

const idFromPathParts: UrlActions.Methods.IdFromPathParts = (
  action: UrlActions.Union,
  ...params: any[]
): Model.ID => {
  const parts = fromPathPartsParseParams(params);

  return parts[+action & +UrlActions.Trash ? 1 : 0];
};

function isPathPart(part: string): boolean {
  return !!UrlActions.fromString.uncapitalized(part) || isValidUUID(part);
}

function extractPathParts(
  pathname: string,
  pathPrefix?: string
): UrlActions.PathParts {
  const from = pathPrefix
    ? pathname.substring(pathPrefix.length + 1)
    : pathname;

  let r = from.split('/').pack();

  if (!pathPrefix) r = r.slice(-PathParts.max).filter(isPathPart);

  return r as UrlActions.PathParts;
}

function parsePath(pathname: string): UrlActions.RouteItems {
  const r: UrlActions.RouteItems = new Map<string, UrlActions.RouteItem>();

  const parts = pathname.split('/').pack();
  let name: string, action: UrlActions, id: string;

  function add() {
    if (!action && !id) return;

    if ((!action || !(+action & uaWithId)) && id)
      action = !action
        ? UrlActions.View
        : UrlActions.fromValue(+action | +UrlActions.View);

    r.set(name, { action, ...(id ? { id } : {}) });

    action = undefined;
    id = undefined;
  }

  for (let l = parts.length, i = 0; i < l; i++) {
    const p = parts[i];
    const a = UrlActions.fromString.uncapitalized(p);

    if (a) action = !action ? a : UrlActions.fromValue(+action | +a);
    else if (isValidUUID(p)) id = p;
    else {
      add();
      name = p;
    }
  }

  add();

  return r;
}

const uaWithPathAndId =
  _.UrlActions.Edit | _.UrlActions.Delete | _.UrlActions.Restore;
const uaWithId = uaWithPathAndId | _.UrlActions.View;
const uaWithPath = uaWithPathAndId | _.UrlActions.Trash | _.UrlActions.New;

// A.V.Stantso:
//   !=  UrlActions.PathPostfixFunc
function toPathPostfix(
  action: UrlActions.Union = UrlActions.Root,
  pathname?: string,
  idOrEd?: Model.ID | Model.IDed
): string {
  const parts = UrlActions._._values
    .reduce((r, v) => {
      if (+action & v)
        r.push(
          uaWithId & v && (JS.is.string(idOrEd) ? idOrEd : idOrEd?.id),
          uaWithPath & v && toPath(v)
        );

      return r;
    }, [] as string[])
    .pack();

  if (pathname || parts.length) parts.unshift(pathname || '');

  return parts.join('/');
}

const replaceInPath: UrlActions.Methods.ReplaceInPath = (
  pathname,
  ...params: any[]
) => {
  const pathPrefix: string = params.length > 1 && params[0];
  const to: UrlActions.Methods.ReplaceInPath.To =
    params[params.length > 1 ? 1 : 0];

  const parts = pathname.split('/').pack();

  let replaceStart = pathPrefix
    ? pathPrefix.split('/').pack().length
    : parts.length - PathParts.max;
  while (
    !pathPrefix &&
    replaceStart < parts.length &&
    !isPathPart(parts[replaceStart])
  )
    replaceStart++;

  let replaceEnd = replaceStart;
  while (replaceEnd < parts.length && isPathPart(parts[replaceEnd]))
    replaceEnd++;

  const replacedParts = parts.slice(replaceStart, replaceEnd);

  const from = fromPathParts(replacedParts);
  const id = idFromPathParts(from, replacedParts);

  const t = +(Perform(to, from, id) || UrlActions.Root);

  parts.splice(
    replaceStart,
    replaceEnd - replaceStart + 1,
    ...toPathPostfix(t, '', id).split('/').pack()
  );

  parts.unshift('');

  return parts.join('/');
};

function editOrSystemicView(
  pathname: string,
  iid: Model.IDed,
  editable: boolean,
  inTrash: boolean
) {
  return toPathPostfix(
    inTrash
      ? UrlActions.fromValue(+UrlActions.Trash | +UrlActions.View)
      : !editable || Model.Systemic.Force(iid).system
      ? UrlActions.View
      : UrlActions.Edit,
    pathname,
    iid.id
  );
}

/**
 * @summary Can access with read rights
 */
function isRead(action: UrlActions.Union) {
  return UrlActions.is(action, UrlActions.Root, UrlActions.View);
}

// @ts-ignore: Excessive stack depth comparing types
// 'Customized<TEnumType, ?, TCustom, TWrap, TWraps>'
// and
// 'Customized<TEnumType, ?, TCustom, TWrap, TWraps>'.ts(2321)
export const UrlActions = Enum(_.UrlActions, 'UrlActions', ({ fromValue }) => ({
  toPath,
  PathParts,
  fromPathParts,
  idFromPathParts,
  isPathPart,
  extractPathParts,
  parsePath,
  toPathPostfix,
  replaceInPath,
  editOrSystemicView,
  isRead,

  /**
   * @summary Every `UrlActions` in this set has `id` url path part
   */
  WithId: fromValue(uaWithId),

  /**
   * @summary Every `UrlActions` in this set has named url path part
   */
  WithPath: fromValue(uaWithPath),
}))<UrlActions.TypeMap>({
  wrapClass: UrlActionsValue,
  combinable: true,
});

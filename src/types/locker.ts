export interface Locker {
  readonly locked: boolean;
  setLocked: React.Dispatch<React.SetStateAction<boolean>>;
  lock(): void;
  unlock(): void;
  toggle(): void;
}

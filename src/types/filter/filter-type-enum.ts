export enum FilterType {
  text = 1,
  set,
  bool,
  custom,
}

export namespace FilterType {
  export type Type = typeof FilterType;

  export namespace Key {
    export type List = ['text', 'set', 'bool', 'custom'];
  }

  export type Key = keyof Type;
  export type Value = Type[Key];
}

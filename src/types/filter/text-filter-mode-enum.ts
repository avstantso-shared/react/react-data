export enum TextFilterMode {
  simple = 1,
  regExp,
}

export namespace TextFilterMode {
  export type Type = typeof TextFilterMode;

  export namespace Key {
    export type List = ['simple', 'regExp'];
  }

  export type Key = keyof Type;
  export type Value = Type[Key];
}

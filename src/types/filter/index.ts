import type React from 'react';

import { Enum, Perform } from '@avstantso/node-or-browser-js--utils';

import type { Caption, DebugLabel, Indexed, PerformCaptions } from '../misc';
import type { Nested } from '../nested';

import * as _ft from './filter-type-enum';
import * as _tfm from './text-filter-mode-enum';

type StdSet<T> = Set<T>;

export namespace Filter {
  export namespace Type {
    // @ts-ignore fix CI TS2589: Type instantiation is excessively deep and possibly infinite
    export type TypeMap = Enum.TypeMap.Make<
      _ft.FilterType.Type,
      _ft.FilterType.Key.List
    >;
  }

  export type Type = Enum.Value<Type.TypeMap>;

  export type GetFiled<TItem = any, TValue = any> = (item: TItem) => TValue;

  export type Filed<TItem = any, TValue = any> =
    | GetFiled<TItem, TValue>
    | string;

  export type List<TItem> = Filter<TItem>[];

  export namespace Text {
    export namespace Mode {
      export type TypeMap = Enum.TypeMap.Make<
        _tfm.TextFilterMode.Type,
        _tfm.TextFilterMode.Key.List
      >;
    }

    export type Mode = Enum.Value<Mode.TypeMap>;

    export interface Extender {
      readonly value: string;
      setValue: React.Dispatch<React.SetStateAction<string>>;
      readonly mode: Mode;
      setMode: React.Dispatch<React.SetStateAction<Mode>>;
      readonly error: Error;
    }
  }

  export interface Text<TItem = any>
    extends Filter<TItem, string>,
      Text.Extender {}

  export namespace Set {
    export namespace Array {
      export interface Options<TSourceItem> {
        captions?: PerformCaptions;
        matcher?: (a: TSourceItem, b: TSourceItem) => boolean;
      }
    }

    export interface Extender {
      readonly captions: ReadonlyArray<Caption>;
      readonly values: StdSet<number>;
      setValues: React.Dispatch<React.SetStateAction<StdSet<number>>>;
    }
  }

  export interface Set<TItem = any, TValue = number>
    extends Filter<TItem, TValue>,
      Set.Extender {}

  export namespace Bool {
    export type Captions = Readonly<{
      false: Perform<Caption>;
      true: Perform<Caption>;
    }>;
  }

  export interface Bool<TItem = any> extends Set<TItem, boolean> {}

  export namespace Custom {
    export type Render<TProps = any> = (props: TProps) => JSX.Element;

    export type Extender<
      TItem = any,
      TValue = any,
      TExtender extends object = {}
    > = Pick<Filter<TItem, TValue>, 'active' | 'clear' | 'filter'> &
      TExtender & {
        render: Render;
      };
  }

  export type Custom<
    TItem = any,
    TValue = any,
    TExtender extends object = {}
  > = Filter<TItem, TValue> & Custom.Extender & TExtender;
}

export interface Filter<TItem = any, TValue = any> extends Indexed {
  readonly type: Filter.Type;
  readonly id: string;
  readonly active: boolean;
  isOpen: boolean;
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
  readonly count: number;
  field: Filter.Filed<TItem, TValue>;
  clear(): void;
  filter(value: TValue): boolean;
}

export namespace Filters {
  export interface Options<TItemOrUnion = any> extends DebugLabel.Option {
    nested?: Nested.Writable<TItemOrUnion, TItemOrUnion>[];
  }

  export namespace Methods {
    export type Clear = () => void;
    export type Filter<TItemOrUnion = any> = <T extends Partial<TItemOrUnion>>(
      list: T[],
      doNotCopyUnfiltered?: boolean
    ) => T[];

    export type UseText<TItemOrUnion = any> = (
      field: Filter.Filed<TItemOrUnion, string>
    ) => Filter.Text<TItemOrUnion>;

    /**
     * @default format 'DD.MM.yyyy'
     */
    export type UseDate<TItemOrUnion = any> = (
      field: Filter.Filed<TItemOrUnion, Date>,
      format?: string
    ) => Filter.Text<TItemOrUnion>;

    export type UseNumber<TItemOrUnion = any> = (
      field: Filter.Filed<TItemOrUnion, number>
    ) => Filter.Text<TItemOrUnion>;

    export type UseCurrency<TItemOrUnion = any> = (
      field: Filter.Filed<TItemOrUnion, number>
    ) => Filter.Text<TItemOrUnion>;

    export type UseFlags<TItemOrUnion = any> = <TValue extends number>(
      field: Filter.Filed<TItemOrUnion, TValue>,
      captions: PerformCaptions
    ) => Filter.Set<TItemOrUnion, TValue>;

    export type UseSet<TItemOrUnion = any> = <
      TValue extends TSourceItem | TSourceItem[],
      TSourceItem
    >(
      field: Filter.Filed<TItemOrUnion, TValue>,
      source: Perform<ReadonlyArray<TSourceItem>>,
      options?: Filter.Set.Array.Options<TSourceItem>
    ) => Filter.Set<TItemOrUnion, TValue>;

    export type UseBool<TItemOrUnion = any> = (
      field: Filter.Filed<TItemOrUnion, boolean>,
      captions: Filter.Bool.Captions
    ) => Filter.Bool<TItemOrUnion>;

    export type UseCustom<TItemOrUnion = any> = <
      TValue = any,
      TExtender extends object = {}
    >(
      field: Filter.Filed<TItemOrUnion, TValue>,
      extender: Filter.Custom.Extender & TExtender
    ) => Filter.Custom<TItemOrUnion, TValue, TExtender>;
  }
}

export interface Filters<TItemOrUnion = any> {
  readonly filters: Filter.List<TItemOrUnion>;

  /**
   * @summary if `dataSeria` changes, the result of filtering the data may change
   */
  readonly dataSeria: number;

  clear: Filters.Methods.Clear;
  filter: Filters.Methods.Filter<TItemOrUnion>;

  useText: Filters.Methods.UseText<TItemOrUnion>;
  useDate: Filters.Methods.UseDate<TItemOrUnion>;
  useNumber: Filters.Methods.UseNumber<TItemOrUnion>;
  useCurrency: Filters.Methods.UseCurrency<TItemOrUnion>;
  useFlags: Filters.Methods.UseFlags<TItemOrUnion>;
  useSet: Filters.Methods.UseSet<TItemOrUnion>;
  useBool: Filters.Methods.UseBool<TItemOrUnion>;

  useCustom: Filters.Methods.UseCustom<TItemOrUnion>;
}

export const Filter = {
  Type: Enum(_ft.FilterType, 'Filter.Type')<Filter.Type.TypeMap>(),
  Text: {
    Mode: Enum(
      _tfm.TextFilterMode,
      'Filter.Text.Mode'
    )<Filter.Text.Mode.TypeMap>(),
  },
};

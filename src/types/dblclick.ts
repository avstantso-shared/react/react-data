export interface DblClickController {
  click(func: () => any): void;
  dblClick(func: () => any): void;
}

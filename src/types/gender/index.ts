import { Enum, EnumWrap } from '@avstantso/node-or-browser-js--utils';

import { T } from '@i18n-utils';
import { LOCALES } from '@i18n';

import * as _ from './enum';

export namespace Gender {
  export type TypeMap = Enum.TypeMap.Make<
    _.Gender.Type,
    _.Gender.Key.List,
    {
      localize(t: T): string;
    }
  >;

  export type Union = Enum.Value.Union<TypeMap>;
}

export type Gender = Enum.Value<Gender.TypeMap>;

function localize(gender: Gender.Union, t: T): string {
  return t(LOCALES.gender[Gender.toName(gender) as keyof object]);
}

class GenderValue extends EnumWrap<Gender.TypeMap> {
  constructor(value: Gender.TypeMap['Value']) {
    super(value);
    Object.setPrototypeOf(this, GenderValue.prototype);
  }

  public localize(t: T): string {
    return localize(this.value, t);
  }
}

export const Gender = Enum(_.Gender, 'Gender', { localize })<Gender.TypeMap>(
  GenderValue
);

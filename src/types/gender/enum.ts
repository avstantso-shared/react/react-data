export enum Gender {
  male = 1,
  female,
  neuter,
}

export namespace Gender {
  export type Type = typeof Gender;

  export namespace Key {
    export type List = ['male', 'female', 'neuter'];
  }

  export type Key = keyof Type;
  export type Value = Type[Key];
}

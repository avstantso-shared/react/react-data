import type { Perform } from '@avstantso/node-or-browser-js--utils';
import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type { IdsSet } from './idsSetContainer';

export namespace Folding {
  export interface Controller<T extends Model.IDed = Model.IDed> {
    entityGetFolding(entity: T): boolean;
    entitySetFolding(entity: T, isFoldedOut: boolean): void;
  }

  export interface ToggleForOverload<T extends Model.IDed> {
    (isFoldedOut: boolean, ...items: (T | Model.IDed | Model.ID)[]): void;
    (...items: (T | Model.IDed | Model.ID)[]): void;
  }

  export interface ToggleAllOverload<T extends Model.IDed> {
    (isFoldedOut: boolean, allItems: Perform<ReadonlyArray<T>>): void;
    (allItems: Perform<ReadonlyArray<T>>): void;
  }
}

export interface Folding<T extends Model.IDed = Model.IDed>
  extends IdsSet.Container,
    Folding.Controller<T> {
  toggleFor: Folding.ToggleForOverload<T>;
  toggleAll: Folding.ToggleAllOverload<T>;
  filter(allItems: Perform<ReadonlyArray<T>>): T[];
}

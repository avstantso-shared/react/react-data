import { BaseError } from '@avstantso/node-or-browser-js--errors';
import { JS } from '@avstantso/node-or-browser-js--utils';
import type { DebugSentry } from '../../types';

export class DebugSentryError extends BaseError {
  label: string;
  depsDiffs: DebugSentry.DependencyDiff[];

  constructor(
    message: string,
    label: string,
    depsDiffs: DebugSentry.DependencyDiff[],
    internal?: Error
  ) {
    super(message, internal);

    this.label = label;
    this.depsDiffs = depsDiffs;

    Object.setPrototypeOf(this, DebugSentryError.prototype);
  }

  static is(error: unknown): error is DebugSentryError {
    return JS.is.error(this, error);
  }
}

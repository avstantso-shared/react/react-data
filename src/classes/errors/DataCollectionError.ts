import { BaseError } from '@avstantso/node-or-browser-js--errors';
import { JS } from '@avstantso/node-or-browser-js--utils';

export class DataCollectionError extends BaseError {
  constructor(message: string, internal?: Error) {
    super(message, internal);

    Object.setPrototypeOf(this, DataCollectionError.prototype);
  }

  static is(error: unknown): error is DataCollectionError {
    return JS.is.error(this, error);
  }
}

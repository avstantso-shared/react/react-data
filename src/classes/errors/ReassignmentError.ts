import { BaseError } from '@avstantso/node-or-browser-js--errors';
import { JS } from '@avstantso/node-or-browser-js--utils';

export class ReassignmentError extends BaseError {
  constructor(message: string = 'Reassignment error', internal?: Error) {
    super(message, internal);

    Object.setPrototypeOf(this, ReassignmentError.prototype);
  }

  static is(error: unknown): error is ReassignmentError {
    return JS.is.error(this, error);
  }
}

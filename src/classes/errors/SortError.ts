import { BaseError } from '@avstantso/node-or-browser-js--errors';
import { JS } from '@avstantso/node-or-browser-js--utils';

export class SortError extends BaseError {
  fieldName: string;

  constructor(message: string, fieldName: string, internal?: Error) {
    super(message, internal);

    this.fieldName = fieldName;

    Object.setPrototypeOf(this, SortError.prototype);
  }

  static is(error: unknown): error is SortError {
    return JS.is.error(this, error);
  }
}

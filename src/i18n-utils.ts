import { useMemo } from 'react';

import i18next, { KeyPrefix, FlatNamespace } from 'i18next';
import * as ReactI18next from 'react-i18next';

import { JS, Generics } from '@avstantso/node-or-browser-js--utils';
import { LocalString } from '@avstantso/node-or-browser-js--model-core';

export type T = typeof i18next.t;
export type TSub = T & { root: T };

export type I18nResources<PkgName extends string, En, Ru> = {
  en: { [K in PkgName]: En };
  ru: { [K in PkgName]: Ru };
};
export function MakeI18nextResources<PkgName extends string, En, Ru>(
  name: PkgName,
  en: En,
  ru: Ru
): I18nResources<PkgName, En, Ru> {
  return {
    en: JS.set.raw({}, name, en),
    ru: JS.set.raw({}, name, ru),
  } as I18nResources<PkgName, En, Ru>;
}

export type GetLocalString = (localesPath: string | object) => LocalString;
export function MakeGetLocalString<En, Ru>(
  localesEn: En,
  localesRu: Ru
): GetLocalString {
  return (localesPath: string | object) => {
    let en = localesEn;
    let ru = localesRu;

    const parts = `${localesPath}`.split('.');
    for (let i = 0; i < parts.length; i++) {
      const part = parts[i];

      en = JS.get.raw(en || {}, part);
      ru = JS.get.raw(ru || {}, part);

      if (!en && !ru) break;
    }

    return en || ru ? Generics.Cast.To({ en, ru }) : undefined;
  };
}

export type $Tuple<T> = readonly [T?, ...T[]];

export type UseTranslation = <
  Ns extends FlatNamespace | $Tuple<FlatNamespace> | undefined = undefined,
  KPrefix extends KeyPrefix<ReactI18next.FallbackNs<Ns>> = undefined
>(
  ns?: Ns,
  options?: ReactI18next.UseTranslationOptions<KPrefix>
) => ReactI18next.UseTranslationResponse<ReactI18next.FallbackNs<Ns>, KPrefix>;

export type UseTranslationSub = <
  Ns extends FlatNamespace | $Tuple<FlatNamespace> | undefined = undefined,
  KPrefix extends KeyPrefix<ReactI18next.FallbackNs<Ns>> = undefined
>(
  parentKey: string,
  ns?: Ns,
  options?: ReactI18next.UseTranslationOptions<KPrefix>
) => TSub;

export interface UseTranslationEx extends UseTranslation {
  sub: UseTranslationSub;
}

let useTranslation0: UseTranslation;
export const i18next_setUseTranslation = (aUseTranslation: UseTranslation) =>
  (useTranslation0 = aUseTranslation);

export function MakeUseTranslation(defaultNS: string): UseTranslationEx {
  function f<
    Ns extends FlatNamespace | $Tuple<FlatNamespace> | undefined = undefined,
    KPrefix extends KeyPrefix<ReactI18next.FallbackNs<Ns>> = undefined
  >(
    ns?: Ns,
    options?: ReactI18next.UseTranslationOptions<KPrefix>
  ): ReactI18next.UseTranslationResponse<ReactI18next.FallbackNs<Ns>, KPrefix> {
    return useTranslation0<Ns, KPrefix>(
      Generics.Cast.To(ns || defaultNS),
      options
    );
  }

  f.sub = function <
    Ns extends FlatNamespace | $Tuple<FlatNamespace> | undefined = undefined,
    KPrefix extends KeyPrefix<ReactI18next.FallbackNs<Ns>> = undefined
  >(
    parentKey: string,
    ns?: Ns,
    options?: ReactI18next.UseTranslationOptions<KPrefix>
  ): TSub {
    const [t] = f(ns, options);
    return useMemo(() => {
      // fix for react-monorepo
      // const fSub: TSub = (...params: any[]) => {
      const fSub = (...params: any[]) => {
        const [key, ...rest] = params;
        return Generics.Cast(t)(`${parentKey}.${key}`, ...rest);
      };
      fSub.root = t;
      return Generics.Cast(fSub);
    }, [parentKey, ns, options, t]);
  };

  return f;
}

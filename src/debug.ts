import debug, { Debugger } from 'debug';

import { Extend } from '@avstantso/node-or-browser-js--utils';

type DebugEx = Debugger & {
  subNameSpace<T extends DebugEx>(subNameSpace: string): T;
};

function DebugEx<T extends DebugEx>(prefix: string = ''): T {
  return new Proxy(debug(prefix), {
    get(target, p) {
      return 'subNameSpace' === p
        ? (subNameSpace: string) => DebugEx(`${prefix}${subNameSpace}`)
        : target[p as keyof typeof target];
    },
  }) as T;
}

const dbg = DebugEx(`@avstantso/rd:`);
const dataCollection = dbg.subNameSpace(`d-c:`);
const dataCollectionTrigger = dataCollection.subNameSpace(`trg:`);

export default Extend.Arbitrary(dbg, {
  dataCollection: Extend.Arbitrary(dataCollection, {
    trigger: dataCollectionTrigger,
  }),
});

import React, { Dispatch, useEffect } from 'react';
import Bluebird from 'bluebird';

import { Generics, Stub, X } from '@avstantso/node-or-browser-js--utils';

import { StateRec, SetStateAction } from '@types';

import { useIncrement } from '@hooks/use-increment';
import { useInitRef } from '@hooks/use-init-ref';

/**
 * @summary Async action state management
 */
export namespace AsyncAction {
  export namespace State {
    /**
     * @summary Async action state for `loading`
     */
    export type Loading = StateRec<'loading', boolean>;

    /**
     * @summary Async action state for `lastError`
     */
    export type LastError = StateRec<'lastError', Error>;

    /**
     * @summary Raw async action state: `loading` & `lastError`
     */
    export type Raw = State.Loading & State.LastError;
  }

  export namespace HighLevelMethods {
    export namespace Exec {
      /**
       * @summary Full async action wrap
       */
      export type Method = <T = void, R = T | void>(
        action: () => Promise<T>,
        onFulfilled?: (actionResult: T) => R | Promise<R>,
        onCustomRejected?: (error: any) => unknown
      ) => Promise<R>;

      /**
       * @summary Full async action safe wrap
       * @param safe `React` component protection from set state for unmounted component. Function stub maker
       */
      export type Safe = (safe: Stub.Overload<false>) => Method;
    }

    /**
     * @summary Full async action safe wrap
     * @param safe `React` component protection from set state for unmounted component. Function stub maker
     */
    export type Exec = Exec.Safe & {
      /**
       * @summary Full async action unsafe version
       */
      exec: Exec.Method;
    };
  }

  /**
   * @summary Async action state high level methods dispatch
   */
  export type HighLevelMethods = {
    /**
     * @summary Start async action = `setLoading(true); setLastError(null);`
     */
    start(): Promise<void>;

    /**
     * @summary End async action = `setLoading(false);`
     */
    end(): void;

    /**
     * @summary Full async action wrap
     * @example
     *   Exec(safe)(action, fulfilled, customRejected);
     *
     *   // Equivalent
     *   safe(start)().then(safe(action)).then(safe(onFulfilled),(e) => {
     *       safe(setLastError)(e);
     *       safe(customRejected)(e);
     *   })).finally(safe(end));
     */
    Exec: HighLevelMethods.Exec;

    /**
     * @summary Synchronize `loading` with `externalLoadingValue` in effect
     * @param externalLoadingValue Actual external `loading` value
     * @example
     *   asyncAction.useSyncLoadingEffect(extLoading);
     *
     *   // Equivalent
     *   useEffect(() => {
     *     asyncAction.setLoading(extLoading);
     *   }, [extLoading]);
     */
    useSyncLoadingEffect(externalLoadingValue: boolean): void;
  };

  /**
   * @summary Async action state
   */
  export type State = {
    [K in keyof State.Raw]: State.Raw[K];
  } & HighLevelMethods;

  /**
   * @summary Async action readonly part
   */
  export type Readonly = Pick<State, 'loading' | 'lastError'>;

  export namespace Readonly {
    /**
     * @summary Async action readonly part `loading`
     */
    export type Loading = Pick<State, 'loading'>;

    /**
     * @summary Async action readonly part `lastError`
     */
    export type LastError = Pick<State, 'lastError'>;
  }

  /**
   * @summary Async action state dispatch part
   */
  export type Dispatch = Pick<State, 'setLoading' | 'setLastError'> &
    HighLevelMethods;

  /**
   * @summary Async action state ref for additional wraps
   * @param TExt Extension for additional wrap
   */
  export type Ref<TExt extends object = {}> = {
    state: State;
  } & TExt;

  export namespace Ref {
    /**
     * @summary Async action state ref hook for additional wraps
     * @param TExt Extension for additional wrap
     */
    export namespace Hook {
      /**
       * @summary Async action state ref hook result for additional wraps
       * @param TExt Extension for additional wrap
       */
      export type Result<TExt extends object = {}> = [
        React.MutableRefObject<Ref<TExt>>,
        boolean
      ];
    }

    export type Hook = <TExt extends object = {}>(
      initialLoading: boolean,
      onSetError?: (error: Error) => unknown
    ) => Hook.Result<TExt>;
  }

  /**
   * @summary Type of `AsyncAction` const
   */
  export namespace Definition {
    /**
     * @summary Get `asyncActionDispatch.Exec` or create default `Exec`
     */
    export type Exec = (
      asyncActionDispatch: AsyncAction.Dispatch
    ) => AsyncAction.Dispatch['Exec'];
  }

  export type Definition = {
    /**
     * @summary Async action "reducer" initial state
     */
    initialState: Readonly;

    /**
     * @summary Errors ignored by `setLastError`
     */
    ignoredErrors: typeof ignoredErrors;

    /**
     * @summary Async action ref hook
     */
    useRef: typeof useAsyncActionRef;

    /**
     * @summary Async action hook
     */
    use: typeof useAsyncAction;

    /**
     * @summary Async action empty structure
     */
    empty: State;

    /**
     * @summary Async action state dispatch part
     */
    Dispatch: {
      /**
       * @summary Async action empty Dispatch structure
       */
      empty: Dispatch;
    };

    /**
     * @summary Get `asyncActionDispatch.Exec` or create default `Exec`
     */
    Exec: Definition.Exec;
  };
}

export type AsyncAction = AsyncAction.State;

const initialState: AsyncAction.Readonly = {
  loading: true,

  // A.V.Stantso:
  // null for React expressions like this:
  //   {lastError && <>{lastError.message}</>}
  lastError: null,
};

const ignoredErrors: ((e: any) => boolean)[] = [];

function ExecFactory({
  setLastError,
  start = Bluebird.resolve,
  end,
}: Partial<
  Pick<AsyncAction, 'setLastError' | 'start' | 'end'>
> = {}): AsyncAction['Exec'] {
  const Exec: AsyncAction.HighLevelMethods.Exec.Safe =
    (safe) => (action, onFulfilled, onCustomRejected) =>
      Generics.Cast(
        safe(start)()
          .then(safe(action))
          .then(safe(onFulfilled || ((r: any) => r)), (e) => {
            safe(setLastError)(e);
            safe(onCustomRejected)(e);
          })
          .finally(safe(end))
      );

  return Object.defineProperties(Exec, {
    exec: { get: () => Exec(Stub) },
  }) as AsyncAction['Exec'];
}

/**
 * @summary Async action ref hook
 */
export const useAsyncActionRef: AsyncAction.Ref.Hook = <
  TExt extends object = {}
>(
  initialLoading = true,
  onSetError?: (error: Error) => unknown
): AsyncAction.Ref.Hook.Result<TExt> => {
  const [ref, initial] = useInitRef(() => ({
    state: {
      ...initialState,
      loading: initialLoading,
    } as AsyncAction,
  })) as AsyncAction.Ref.Hook.Result<TExt>;

  const inc = useIncrement();

  if (initial) {
    const dispatch: Dispatch<SetStateAction<AsyncAction.Readonly>> = (
      param
    ) => {
      const { loading, lastError } = SetStateAction.resolve(param);
      const { state } = ref.current;

      const chLoading = loading !== state.loading;
      const chLastError = lastError !== state.lastError;
      if (!(chLoading || chLastError)) return;

      chLastError && onSetError && onSetError(lastError);

      ref.current.state = { ...state, loading, lastError };
      inc();
    };

    const setLoading: AsyncAction['setLoading'] = (param) => {
      const loading = SetStateAction.resolve(param);
      const { lastError } = ref.current.state;
      return dispatch({ loading, lastError });
    };

    const setLastError: AsyncAction['setLastError'] = (param) => {
      const { loading } = ref.current.state;
      const lastError = SetStateAction.resolve(param);

      if (ignoredErrors.some((item) => item(lastError))) return;

      return dispatch({ loading, lastError });
    };

    const start: AsyncAction['start'] = () =>
      Bluebird.resolve(dispatch(initialState));

    const end: AsyncAction['end'] = () => setLoading(false);

    const useSyncLoadingEffect: AsyncAction['useSyncLoadingEffect'] = (
      loading
    ) => {
      useEffect(() => {
        setLoading(loading);
      }, [loading]);
    };

    ref.current.state = {
      ...ref.current.state,
      setLoading,
      setLastError,
      start,
      end,
      Exec: ExecFactory({ setLastError, start, end }),
      useSyncLoadingEffect,
    };
  }

  return [ref, initial];
};

/**
 * @summary Async action hook
 */
export function useAsyncAction(
  initialLoading = true,
  onSetError?: (error: Error) => unknown
): AsyncAction {
  const [ref] = useAsyncActionRef(initialLoading, onSetError);
  return ref.current.state;
}

const emptyDispatch: AsyncAction.Dispatch = {
  setLoading: X,
  setLastError: X,
  start: X,
  end: X,
  Exec: ExecFactory(),
  useSyncLoadingEffect: X,
};

const empty: AsyncAction = {
  loading: null,
  lastError: null,
  ...emptyDispatch,
};

export const AsyncAction: AsyncAction.Definition = {
  initialState,
  ignoredErrors,
  useRef: useAsyncActionRef,
  use: useAsyncAction,
  empty,
  Dispatch: {
    empty: emptyDispatch,
  },
  Exec: (asyncActionDispatch: AsyncAction.Dispatch) =>
    asyncActionDispatch?.Exec || ExecFactory(asyncActionDispatch),
};

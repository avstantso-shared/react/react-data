import React, { useLayoutEffect, useRef } from 'react';

import { Perform } from '@avstantso/node-or-browser-js--utils';

import { DebugSentry } from '@types';
import { DebugSentryError } from '@classes';

function emptyDebugSentry(): DebugSentry {
  return {
    frame: null,
    lastSnapshot: null,
    immediateUpdates: null,
    update: () => null,
  };
}

function useDebugSentry(options?: DebugSentry.Options): DebugSentry {
  const { label } = options || {};

  const ref = useRef({ frame: 0, immediateUpdates: 0 } as DebugSentry);

  useLayoutEffect(() => {
    ref.current = { ...ref.current, frame: ref.current.frame + 1 };
  });

  const update = (deps: React.DependencyList, warnThreshold = 10) => {
    const {
      frame,
      lastSnapshot,
      immediateUpdates: oldImmediateUpdates,
    } = ref.current;

    let immediateUpdates = 0;
    if (lastSnapshot) {
      const framesDiff = frame - lastSnapshot.frame;
      if (1 === framesDiff) {
        immediateUpdates = oldImmediateUpdates + 1;
        if (immediateUpdates > warnThreshold) {
          const depsDiffs: DebugSentry.DependencyDiff[] = [];
          for (
            let i = 0;
            i < Math.min(lastSnapshot.deps.length, deps.length);
            i++
          ) {
            if (lastSnapshot.deps[i] === deps[i]) continue;

            depsDiffs.push({
              index: i,
              old: lastSnapshot.deps[i],
              new: deps[i],
            });
          }

          const labelValue = Perform(label) || '<No label>';
          const title = `DebugSentry: immediate updates threshold exceeded for "${labelValue}"`;

          console.warn(`${title}. Dependency difference is %O`, depsDiffs);

          throw new DebugSentryError(title, labelValue, depsDiffs);
        }
      }
    }

    ref.current = {
      ...ref.current,
      lastSnapshot: { frame, deps },
      immediateUpdates,
    };
  };

  return { ...ref.current, update };
}

export default process.env.DEBUG_INFO_ENABLED
  ? useDebugSentry
  : emptyDebugSentry;

import { useState } from 'react';

import type { Locker } from '@types';

export function useLocker(initialLocked?: boolean): Locker {
  const [locked, setLocked] = useState(initialLocked);

  const lock = () => setLocked(true);
  const unlock = () => setLocked(false);
  const toggle = () => setLocked((prev) => !prev);

  return {
    get locked() {
      return locked;
    },
    set locked(value) {
      setLocked(value);
    },
    setLocked,
    lock,
    unlock,
    toggle,
  };
}

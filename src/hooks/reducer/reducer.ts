import React from 'react';

import { Generics, JS } from '@avstantso/node-or-browser-js--utils';
import { InvalidArgumentError } from '@avstantso/node-or-browser-js--errors';
import _ from 'lodash';

import { useIncrement } from '../use-increment';

import { SetStateAction } from '@types';

/**
 * @see `Reducer` from `./index.ts` comment
 */
export function Reducer<TState extends object>(
  initialState?: TState,
  initializer?: (arg: TState) => TState
) {
  //#region types
  type Field = keyof TState;

  type Reduce<TField extends Field, TValueType = TState[TField]> = (
    state: TState,
    value: TValueType
  ) => TState;

  type FResultBase<
    TField extends Field = Field,
    TIsFuncField extends boolean = boolean,
    TValueType = TState[TField]
  > = {
    field: TField;
    reduce: Reduce<TField, TValueType>;
    isFuncField: TIsFuncField;
  };

  type FResultCustom<
    TField extends Field = Field,
    TIsFuncField extends boolean = boolean
  > = <TValueType>(
    reduce: Reduce<TField, TValueType>
  ) => FResultBase<TField, TIsFuncField, TValueType>;

  type FResult<
    TField extends Field = Field,
    TIsFuncField extends boolean = boolean
  > = FResultBase<TField, TIsFuncField> & FResultCustom<TField, TIsFuncField>;

  type F = {
    /**
     * @summary Definer for NOT function fields
     * @param field Field of `TState`
     */
    <TField extends Field>(field: TField): TState[TField] extends Function
      ? 'For function fields use `f.f()`'
      : FResult<TField, false>;

    /**
     * @summary Definer for function fields
     * @param field Field of `TState`
     */
    f<TField extends Field>(
      field: TField
    ): TState[TField] extends Function
      ? FResult<TField, true>
      : 'For NOT function fields use `f()`';
  };

  type RawMeta = NodeJS.Dict<FResultBase<Field, boolean, any>>;
  type Meta<TRaw extends RawMeta = never> = [TRaw] extends [never]
    ? NodeJS.Dict<FResultBase>
    : {
        [K0 in keyof TRaw]: { [K1 in keyof TRaw[K0]]: TRaw[K0][K1] };
      };

  type Payload<
    TMeta extends Meta,
    TMessage extends keyof TMeta = keyof TMeta
  > = Parameters<TMeta[TMessage]['reduce']>[1];

  type Message<
    TMeta extends Meta,
    TMessage extends keyof TMeta = keyof TMeta
  > = {
    msg: TMessage;
    payload?: SetStateAction<Payload<TMeta, TMessage>>;
  };

  type Unsupported<TMeta extends Meta> = (
    message: Pick<Message<TMeta>, 'msg'>
  ) => never;

  type Reducer<TMeta extends Meta> = (
    state: TState,
    message: Message<TMeta>
  ) => TState;

  type DispatchByMessage<
    TMeta extends Meta,
    TMessage extends keyof TMeta,
    TPayload extends Payload<TMeta, TMessage> = Payload<TMeta, TMessage>
  > = React.Dispatch<SetStateAction<TPayload>>;

  type DispatchCommon<TMeta extends Meta> = <TMessage extends keyof TMeta>(
    message: Message<TMeta, TMessage>
  ) => void;

  type Dispatch<TMeta extends Meta> = DispatchCommon<TMeta> & {
    [K in keyof TMeta]: DispatchByMessage<TMeta, K>;
  };

  type DispatchDecorator<TMeta extends Meta> = (
    dispatch: React.Dispatch<Message<TMeta>>
  ) => Dispatch<TMeta>;

  /**
   * @summary `React.useReducer()` wrapper
   */
  type Use<TMeta extends Meta> = () => [TState, Dispatch<TMeta>];

  type Ref<TMeta extends Meta> = { state: TState; dispatch: Dispatch<TMeta> };

  type UseRefRaw<TMeta extends Meta> = {
    /**
     * @summary `React.useReducer()` analog without internal hooks
     * @param storeField Field in `ref.current` for store reducer data
     * @param ref Ref for store reducer data
     * @param initial Sign of initialization need
     * @param notifyReactStateChanges Function for notify `React` about state changes
     */
    <TStoreField extends string>(
      storeField: TStoreField,
      ref: React.MutableRefObject<{ [K in TStoreField]: Ref<TMeta> }>,
      initial: boolean,
      notifyReactStateChanges: () => unknown
    ): ReturnType<Use<TMeta>>;

    /**
     * @summary `React.useReducer()` analog without internal hooks
     * @param ref Ref for store reducer data
     * @param initial Sign of initialization need
     * @param notifyReactStateChanges Function for notify `React` about state changes
     */
    (
      ref: React.MutableRefObject<Ref<TMeta>>,
      initial: boolean,
      notifyReactStateChanges: () => unknown
    ): ReturnType<Use<TMeta>>;
  };

  type UseRef<TMeta extends Meta> = Use<TMeta> & {
    /**
     * @summary `React.useReducer()` analog without internal hooks
     * @param storeField Field in `ref.current` for store reducer data
     * @param ref Ref for store reducer data
     * @param initial Sign of initialization need
     * @param notifyReactStateChanges Function for notify `React` about state changes
     */
    Raw: UseRefRaw<TMeta>;
  };

  type AsCallback<TResult extends RawMeta> = (f: F) => TResult;
  type AsResult<TMeta extends Meta> = {
    /**
     * @summary Meta for use `typeof`.
     *
     * `undefined` in runtime
     */
    meta?: {
      /**
       * @summary Use `typeof meta.data` for get `TMeta` type.
       *
       * `undefined` in runtime
       */
      data: TMeta;

      /**
       * @summary Use `typeof meta.ref` for get `Ref` type.
       *
       * `undefined` in runtime
       */
      ref: Ref<TMeta>;

      /**
       * @summary Use `typeof meta.dispatch` for get `Dispatch` type.
       *
       * `undefined` in runtime
       */
      dispatch: Dispatch<TMeta>;
    };

    /**
     * @summary `initialState` param for `React.useReducer`
     */
    initialState: TState;

    /**
     * @summary  `initializer` param for `React.useReducer`
     * @returns Chain `{as}`. Call `as` to continue chaining
     */
    initializer: (arg: TState) => TState;

    /**
     * @summary Throws `InvalidArgumentError` for `message.msg`
     * @param message Message
     */
    unsupported: Unsupported<TMeta>;

    /**
     * @summary `reducer` param to pass in `React.useReducer`
     * @param state Prev state
     * @param message Message
     */
    reducer: Reducer<TMeta>;

    /**
     * @summary Decorate `dispatch` method returned from `React.useReducer`
     * @param dispatch `React.useReducer(XXX)[1]`
     */
    Dispatch: DispatchDecorator<TMeta>;

    /**
     * @summary `React.useReducer()` wrapper
     */
    useStdReducer: Use<TMeta>;

    /**
     * @summary `React.useReducer()` analog by increment `React.useState()` and `React.useRef()`.
     *
     * No rerender, if `state` equals prev `state`
     */
    useRefReducer: UseRef<TMeta>;
  };

  type AsResultWithCommon<TMeta extends Meta> = AsResult<TMeta> & {
    /**
     * @summary Add common reducer for unhandled cases
     * @param reduce Common reducer
     */
    common(reduce: Reducer<TMeta>): AsResult<TMeta>;
  };
  //#endregion

  function as<TRawMeta extends RawMeta>(
    callback: AsCallback<TRawMeta>
  ): AsResultWithCommon<Meta<TRawMeta>> {
    type M = Meta<TRawMeta>;

    function F(isFuncField = false): F {
      function f<TField extends Field>(field: TField) {
        const fCustom: FResult<TField> = (reduce) => ({
          field,
          isFuncField,
          reduce,
        });

        fCustom.field = field;
        fCustom.isFuncField = isFuncField;
        fCustom.reduce = (state, value) => JS.set({ ...state }, field, value);

        return fCustom;
      }

      if (!isFuncField) f.f = F(true);

      return Generics.Cast.To(f);
    }

    const M: M = callback(F());

    const unsupported: Unsupported<M> = ({ msg }) => {
      throw new InvalidArgumentError(
        `Reducer for message "${String(msg)}" not supported`
      );
    };

    function Reducer(common?: Reducer<M>): Reducer<M> {
      const reducer: Reducer<M> = (state, message) => {
        const handler: FResultBase = JS.get(M, message.msg);

        if (!(handler || common)) return unsupported(message);

        if (handler) {
          const payload = SetStateAction.resolve(
            message.payload,
            JS.get(state, handler.field)
          );

          return handler.reduce(state, Generics.Cast.To(payload));
        }

        return common ? common(state, message) : unsupported(message);
      };

      return reducer;
    }

    const Dispatch: DispatchDecorator<M> = (original) => {
      const dispatch: typeof original = (value) => original(value);

      Object.keys(M).forEach((msg) =>
        Object.defineProperty(dispatch, msg, {
          value(payload: SetStateAction<Payload<M>>) {
            return dispatch({ msg, payload });
          },
        })
      );

      return Generics.Cast.To(dispatch);
    };

    function Result(reduce?: Reducer<M>): AsResult<M> {
      const reducer = Reducer(reduce);

      const useStdReducer: Use<M> = () => {
        const [state, dispatch] = React.useReducer(
          reducer,
          initialState,
          initializer
        );

        return [state, Dispatch(dispatch)];
      };

      const useRefReducerRaw: UseRefRaw<M> = (...params: any[]): any => {
        const hasField = JS.is.string(params[0]) ? 1 : 0;
        const storeField: string = hasField ? params[0] : undefined;

        const ref: React.MutableRefObject<Ref<M> | NodeJS.Dict<Ref<M>>> =
          params[0 + hasField];

        const initial: boolean = params[1 + hasField];
        const increment: () => unknown = params[2 + hasField];

        const proxy = (
          storeField
            ? JS.getOrSet.raw(ref.current, storeField, {})
            : ref.current || (ref.current = {})
        ) as Ref<M>;

        if (initial) {
          const dispatch: React.Dispatch<Message<M>> = (message) => {
            const next = reducer(proxy.state, message);

            if (_.isEqual(proxy.state, next)) return;

            proxy.state = next;
            increment();
          };

          proxy.state = initializer
            ? initializer(proxy.state || initialState)
            : proxy.state || initialState;

          proxy.dispatch = Dispatch(dispatch);
        }

        return [proxy.state, proxy.dispatch];
      };

      const useRefReducer: UseRef<M> = () => {
        const ref = React.useRef<Ref<M>>();

        return useRefReducerRaw(ref, !ref.current, useIncrement());
      };
      useRefReducer.Raw = useRefReducerRaw;

      return {
        initialState,
        initializer,
        unsupported,
        reducer,
        Dispatch,
        useStdReducer,
        useRefReducer,
      };
    }

    return {
      ...Result(),
      common: Result,
    };
  }

  return { as };
}

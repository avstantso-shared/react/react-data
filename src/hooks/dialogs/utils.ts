import * as Dialog from '@root/types/dialogs';

export function convertHandler<TInner, TOuter>(
  handler: Dialog.Entity.Handler<TOuter>,
  converter: (inner: TInner) => TOuter
): Dialog.Entity.Handler<TInner> {
  const cancel = () => handler.cancel();
  const redirect: Dialog.Entity.Redirect = (url) => handler.redirect(url);
  const apply = (entity: TInner) => handler.apply(converter(entity));
  const accept = (entity: TInner) => handler.accept(converter(entity));
  const raw = (result: Dialog.Entity.Result.Closable<TInner>) =>
    handler.raw({ ...result, entity: converter(result.entity) });

  return { cancel, redirect, apply, accept, raw };
}

// Convenient handler call
export function handleDlg<TEntity>(
  handler: Dialog.Entity.Handler.Raw<TEntity>,
  close: () => void
): Dialog.Entity.Handler<TEntity> {
  const cancel = () =>
    handler({
      isClosed: true,
      isCanceled: true,
      close,
    });

  const redirect: Dialog.Entity.Redirect = (url) =>
    handler({
      isClosed: true,
      isCanceled: true,
      redirectUrl: url,
      close,
    });

  const apply = (entity: TEntity) =>
    handler({ isClosed: false, isCanceled: false, entity, close });

  const accept = (entity: TEntity) =>
    handler({ isClosed: true, isCanceled: false, entity, close });

  const raw = (result: Dialog.Entity.Result.Closable<TEntity>) =>
    handler({ ...result, close });

  return {
    cancel,
    redirect,
    apply,
    accept,
    raw,
  };
}

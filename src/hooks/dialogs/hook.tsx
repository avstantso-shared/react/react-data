import React, { useMemo, useRef, useState } from 'react';
import Bluebird from 'bluebird';

import { JS, Perform } from '@avstantso/node-or-browser-js--utils';

import * as Dialog from '@root/types/dialogs';
import { Componented } from '@root/types';

import { handleDlg } from './utils';

/**
 * @summary Entity dialog controller hook
 * @param ComponentBuilder Controled component. Must be immutable for `apply` case
 * @param dialogOptions Controller options
 * @returns Dialog controller
 */
export function useEntityDialog<
  TEntity extends Perform.Able,
  TParams extends Perform.Able = never
>(
  ComponentBuilder: Dialog.Entity.ComponentBuilder<TEntity, TParams>,
  dialogOptions?: Dialog.Entity.Options
): Dialog.Entity<TEntity, TParams> {
  const label = Perform(dialogOptions?.label);

  const [stateRaw, setStateRaw] =
    useState<Dialog.Entity.State<TEntity, TParams>>(null);

  // A.V.Stantso:
  // Variable "state" allows asynchronous elements to work correctly between renderings
  // (relevant for complex use cases of dialog)
  let state = stateRaw;
  const setState: typeof setStateRaw = (param) =>
    setStateRaw((prev) => {
      state = JS.is.function(param) ? param(prev) : param;
      return state;
    });

  let isCloseCalled = false;
  const close = () => {
    if (isCloseCalled) return;

    setState(null);
    isCloseCalled = true;
  };

  const execute: Dialog.Entity.Execute<TEntity, TParams> = (
    callBack,
    options?
  ) => {
    isCloseCalled = false;
    const { entity, params } = options || {};

    let lock = false;
    const handler = (
      result: Dialog.Entity.Result.Closable<TEntity>
    ): Promise<void> => {
      if (lock) return;

      return Bluebird.resolve((lock = true))
        .then(() => {
          if (result.isClosed) close();
        })
        .then(() => callBack(result))
        .tap(() => {
          if (!(result.isClosed || isCloseCalled)) {
            setState({ handler, entity, params });
          }
        })
        .finally(() => (lock = false));
    };

    if (isCloseCalled) return;

    setState({ handler, entity, params });
  };

  const promise: Dialog.Entity.Execute.Promise<TEntity, TParams> = (options) =>
    new Bluebird.Promise<Dialog.Entity.Result<TEntity>>((resolve, reject) =>
      execute((dlgResult) => {
        const { close, ...data } = dlgResult;
        return Bluebird.resolve(resolve(data));
      }, options)
    );

  type Ref = { jsx?: JSX.Element } & Componented;
  const ref = useRef<Ref>();
  if (!ref.current) {
    const Component: React.FC = () => ref.current.jsx;
    ref.current = { Component };
  }

  ref.current.jsx = useMemo(() => {
    if (isCloseCalled || !state) return null;

    return (
      <ComponentBuilder
        handler={handleDlg(state.handler, close)}
        entity={Perform(state.entity)}
        params={Perform(state.params)}
      />
    );
  }, [state]);

  return {
    get isShowed() {
      return !(isCloseCalled || !state);
    },
    execute,
    promise,
    close,
    label,
    Component: ref.current.Component,
  };
}

export function useMessageDialog(
  ComponentBuilder: Dialog.Entity.ComponentBuilder<
    string | JSX.Element,
    string | JSX.Element
  >,
  options?: Dialog.Entity.Options
): Dialog.Message {
  const dlg = useEntityDialog(ComponentBuilder, options);
  const { isShowed, close, label, Component } = dlg;

  const execute = (
    text: string | JSX.Element,
    title?: string | JSX.Element
  ): Promise<boolean> =>
    dlg
      .promise({ entity: text, params: title })
      .then(({ isCanceled }) => !isCanceled);

  return { isShowed, execute, close, label, Component };
}

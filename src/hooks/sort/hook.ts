import { useState } from 'react';
import _ from 'lodash';

import { Generics, JS, Perform } from '@avstantso/node-or-browser-js--utils';

import { Sort, Sorts, Nested } from '@types';

import { SetupComparision } from './comparision';

function sortNested<TItemOrUnion, T extends Partial<TItemOrUnion>>(
  columns: Sort.Column[],
  nested: Nested.Readonly<TItemOrUnion, TItemOrUnion>[],
  comparers: Sort.Comparer.List<T>,
  list: T[],
  level = 0
) {
  list.sort((a, b) => {
    for (let i = 0; i < columns.length; i++) {
      const column = columns[i];
      const comp = comparers[column.colIndex];
      if (!JS.is.function(comp)) continue;

      const r = comp(a, b);
      if (r !== 0) return Sort.Kind.sign(column.sortKind, r);
    }

    return 0;
  });

  if (nested?.length > level)
    list.forEach((item) => {
      const n = nested[level];
      const nestedList: T[] = JS.is.string(n)
        ? JS.get.raw(item, n)
        : Generics.Cast.To(n(Generics.Cast.To(item)));

      sortNested(columns, nested, comparers, nestedList, level + 1);
    });

  return list;
}

export const useSorts: Sort.Methods.UseSorts = <TItemOrUnion>(
  param0: any,
  param1?: any,
  param2?: any
): Sorts<TItemOrUnion> => {
  const initialState: Perform<Sort.Column[] | Sort.Column> = param1 && param0;
  const comparisionSetup: Sort.Comparision.Setup<Partial<TItemOrUnion>> =
    param1 || param0;
  const nested: Nested.Readonly<TItemOrUnion, TItemOrUnion>[] =
    param0 && param1 && (param2 || Array.isArray(param1))
      ? param2 || param1
      : undefined;

  const [columns, setColumns] = useState<Sort.Column[]>(() => {
    const d = Perform(initialState);
    if (!d) return [];

    if (!Array.isArray(d)) return [d];

    return d;
  });

  const byColIndex: Sort.Methods.ByColIndex = (colIndex) =>
    colIndex >= 0
      ? columns.find((item) => item.colIndex === colIndex)
      : undefined;

  const clear: Sort.Methods.Clear = () => setColumns([]);

  const toggle: Sort.Methods.Toggle = (colIndex, multiColumn?) =>
    setColumns((prev) => {
      const i = prev.findIndex((item) => item.colIndex === colIndex);
      if (i < 0)
        return [
          ...(!multiColumn ? [] : prev),
          { colIndex, sortKind: Sort.Kind.asc },
        ];

      const sortKind = Sort.Kind.next(prev[i].sortKind);
      if (Sort.Kind.none === sortKind) {
        if (!multiColumn) return [];
        else {
          prev.splice(i, 1);
          return [...prev];
        }
      } else {
        const nextI = { ...prev[i], sortKind };
        if (!multiColumn) return [nextI];

        const next = [...prev];
        next.splice(i, 1, nextI);
        return next;
      }
    });

  const toggleClick: Sort.Methods.ToggleClick = (colIndex) => (e) => {
    e.stopPropagation();
    return toggle(colIndex, e.ctrlKey);
  };

  const sort: Sort.Methods.Sort<TItemOrUnion> = <
    T extends Partial<TItemOrUnion>
  >(
    list: T[]
  ): T[] => {
    const comparers = SetupComparision<T>(Generics.Cast.To(comparisionSetup));
    return sortNested(columns, nested, comparers, list);
  };

  return {
    columns,
    byColIndex,
    clear,
    toggle,
    toggleClick,
    sort,
  };
};

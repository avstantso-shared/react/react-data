import _ from 'lodash';

import { FIELDS } from '@avstantso/node-or-browser-js--model-core';
import { JS } from '@avstantso/node-or-browser-js--utils';

import { Sort } from '@types';
import { SortError } from '@classes';

export type Add<T> = (fieldName: string, comparer: Sort.Comparer<T>) => unknown;

function Compares<T>(sc: () => Sort.Comparision<T>, add: Add<T>) {
  function Compare<TType>(asc: Sort.Comparer<TType>) {
    function mk(fieldName: string, isDsc?: boolean) {
      add(fieldName, (a: T, b: T): number => {
        const f = fieldName.split('.');

        let fa: any = a,
          fb: any = b;
        for (let i = 0; i < f.length; i++) {
          fa = fa && JS.get.raw(fa, f[i]);
          fb = fb && JS.get.raw(fb, f[i]);
        }

        return undefined === fa && undefined === fb
          ? 0
          : asc(fa, fb) * (isDsc ? -1 : 1);
      });

      return sc();
    }

    mk.Dsc = (fieldName: string) => mk(fieldName, true);

    const base: Sort.Comparision.BaseType<T> = (fieldName: string) =>
      mk(fieldName);
    base.Dsc = (fieldName: string) => mk.Dsc(fieldName);

    function core(fieldName: string): Sort.Comparision.CoreType<T> {
      const f = () => mk(fieldName);
      f.Dsc = () => mk.Dsc(fieldName);

      return f;
    }

    return { base, core };
  }

  return {
    string: Compare<string>((a, b) => (a || '').localeCompare(b || '')),
    number: Compare<number>((a, b) => a - b),
    boolean: Compare<boolean>((a, b) => (a ? 1 : 0) - (b ? 1 : 0)),
    Date: Compare<Date>((a, b) => a?.valueOf() - b?.valueOf()),
    json: Compare<any>((a, b) =>
      ((a && JSON.stringify(a)) || '').localeCompare(
        (b && JSON.stringify(b)) || ''
      )
    ),
  };
}

function SortComparision<T>(add: Add<T>): Sort.Comparision<T> {
  let sc: Sort.Comparision<T>;

  function Custom(): Sort.Comparision.Custom<T> {
    return (param0: any, param1?: any) => {
      const fieldName: string = param1 && param0;
      const comparer: Sort.Comparer<T> = param1 || param0;

      add(fieldName || `<Custom_Comparision>`, comparer);
      return sc;
    };
  }

  const c = Compares(() => sc, add);

  sc = {
    // base types
    string: c.string.base,
    number: c.number.base,
    boolean: c.boolean.base,
    Date: c.Date.base,
    json: c.json.base,

    // core types
    ID: c.string.core(FIELDS.id),
    Named: c.string.core(FIELDS.name),
    Described: c.string.core(FIELDS.description),
    AuthoredLogin: c.string.core(FIELDS.author),
    Activable: c.boolean.core(FIELDS.active),
    Systemic: c.boolean.core(FIELDS.system),
    Moduled: c.string.core(FIELDS.module),
    PutDate: c.Date.core(FIELDS.putdate),
    UpdDate: c.Date.core(FIELDS.upddate),

    // custom
    custom: Custom(),
  };

  return sc;
}

export function SetupComparision<T>(setup: Sort.Comparision.Setup<T>) {
  const result: Sort.Comparer.List<T> = [];

  const add: Add<T> = (fieldName, comparer) =>
    result.push((a: T, b: T): number => {
      try {
        return comparer(a, b);
      } catch (e) {
        throw new SortError(
          `SortError: ${e.message} for field "${fieldName}"`,
          fieldName,
          e
        );
      }
    });

  const sc = SortComparision<T>(add);
  setup(sc);

  return result;
}

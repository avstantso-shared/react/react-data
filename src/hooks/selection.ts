import { useState, useEffect } from 'react';

import { JS } from '@avstantso/node-or-browser-js--utils';
import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type { Folding, Selection, SelectionsSet } from '@types';

import {
  default as IdsSetManagement,
  useIdsSetContainer,
} from '@hooks/idsSetContainer';

const { empty, fromDataList } = IdsSetManagement;

export function useSelection<T extends Model.IDed>(
  allItems: ReadonlyArray<T>,
  folding?: Folding<T>
): Selection<T> {
  const { data, setData, clone, clear } = useIdsSetContainer();

  useEffect(() => {
    setData(empty());
    return () => {
      setData(empty());
    };
  }, []);

  const toggleAll = (isSelected: boolean) => {
    setData(isSelected ? fromDataList(allItems) : empty());
    if (isSelected && folding) folding.toggleAll(true, allItems);
  };

  const toggleFor = (isSelected: boolean, ...items: Model.IDed[] | string[]) =>
    setData((prevState: Set<string>) => {
      items
        .map((item) => (JS.is.string(item) ? item : item.id))
        .forEach((id) =>
          isSelected ? prevState.add(id) : prevState.delete(id)
        );

      if (isSelected && folding) folding.toggleFor(true, ...items);

      return new Set<string>(prevState);
    });

  const getSelected = (entity: T) => data.has(entity.id);
  const toggleSelection = (entity: T, selected: boolean) =>
    setData((prevState: Set<string>) => {
      selected ? prevState.add(entity.id) : prevState.delete(entity.id);

      if (selected && folding) {
        const f = folding.clone();
        f.add(entity.id);
        folding.setData(f);
      }

      return new Set<string>(prevState);
    });

  const controller = () => ({
    getSelected,
    toggleSelection,
  });

  return {
    data,
    setData,
    clone,
    clear,
    toggleAll,
    toggleFor,
    controller,
    items: () => allItems.filter((item) => data.has(item.id)),
  };
}

export function useSelectionsSet(
  selections: ReadonlyArray<Selection.Raw>,
  initialIsAllSelected?: boolean
): SelectionsSet {
  const [isAllSelected, setIsAllSelected] = useState(
    initialIsAllSelected ? true : false
  );
  const [selectionSize, setSelectionSize] = useState(0);

  useEffect(
    () => {
      setSelectionSize(
        selections.reduce((r, item) => (r += item.data.size), 0)
      );
    },
    selections.map((selection) => selection.data)
  );

  const toggleSelectAll = (e?: any) =>
    setIsAllSelected((prev) => {
      const newIsAllSelected = !prev;

      selections.forEach((selection) => selection.toggleAll(newIsAllSelected));

      return newIsAllSelected;
    });

  const clear = () =>
    setIsAllSelected(() => {
      selections.forEach((selection) => selection.toggleAll(false));

      return false;
    });

  return {
    get isAllSelected() {
      return isAllSelected;
    },
    set isAllSelected(value) {
      setIsAllSelected(value);
    },
    selectionSize,
    toggleSelectAll,
    clear,
  };
}

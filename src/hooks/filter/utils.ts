import { JS } from '@avstantso/node-or-browser-js--utils';

import { Filter } from '@types';

export function getFieldValue<TItem, TValue = any>(
  item: TItem,
  field: Filter.Filed<TItem, TValue>
): TValue {
  return JS.is.function(field) ? field(item) : item[field as keyof object];
}

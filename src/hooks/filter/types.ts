import type React from 'react';

import type { Filter } from '@types';

export interface FiltersProps<TItem> {
  readonly idTemplate: React.HTMLProps<HTMLElement>['id'];
  setFilters: React.Dispatch<React.SetStateAction<Filter.List<TItem>>>;
  setDataSeria: React.Dispatch<React.SetStateAction<number>>;
}

export interface FilterInternal<TItem = any, TValue = any>
  extends Filter<TItem, TValue> {
  setCount: React.Dispatch<React.SetStateAction<number>>;
}

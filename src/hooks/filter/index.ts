import { useCallback, useMemo, useState } from 'react';
import _ from 'lodash';

import { Generics, JS } from '@avstantso/node-or-browser-js--utils';

import { Filter, Filters, Nested } from '@types';

import type { FiltersProps, FilterInternal } from './types';
import {
  UseFilterText,
  UseFilterDate,
  UseFilterNumber,
  UseFilterCurrency,
  UseFilterCustom,
  UseFilterFlagsSet,
  UseFilterArraySet,
  UseFilterBool,
} from './filters';
import { getFieldValue } from './utils';

function isItemFilteredFlat<TItem>(
  activeFilters: Filter<TItem, any>[],
  item: TItem
): boolean {
  return activeFilters.every((f) => {
    const value = getFieldValue(item, f.field);

    const r = f.filter(value);

    if (r) (f as FilterInternal).setCount((prev) => prev + 1);

    return r;
  });
}

/**
 * @returns All rows that match the condition or have child rows that match the condition. If child rows do not satisfy the condition, they are discarded
 */
function filterNested<TItem, T extends Partial<TItem>>(
  activeFilters: Filter<TItem, any>[],
  nested: Nested.Writable<Partial<TItem>, Partial<TItem>>[],
  list: T[],
  level = 0
) {
  return list.reduce((rList, item) => {
    const isFlatOk = isItemFilteredFlat(activeFilters, Generics.Cast.To(item));

    function hasNested(): boolean {
      if (!(nested?.length > level)) return false;

      const n = nested[level];
      const nestedList = JS.is.string(n)
        ? item[n as keyof object]
        : n.getter(item);

      const rNested = nestedList
        ? filterNested(
            activeFilters,
            nested,
            Generics.Cast.To(nestedList),
            level + 1
          )
        : [];

      if (!(rNested.length || isFlatOk)) return false;

      const changedItem: any = { ...item };

      JS.is.string(n)
        ? (changedItem[n as keyof object] = rNested)
        : n.setter(changedItem, rNested);

      rList.push(changedItem);

      return true;
    }

    if (!hasNested() && isFlatOk) rList.push(item);

    return rList;
  }, [] as T[]);
}

export function useFilters<TItem>(
  options?: Filters.Options<Partial<TItem>>
): Filters<TItem> {
  const { label, nested } = options || {};

  const idTemplate = useMemo(() => _.uniqueId(), []);
  const [filters, setFilters] = useState<Filter.List<TItem>>([]);
  const [dataSeria, setDataSeria] = useState(0);

  const props: FiltersProps<TItem> = { idTemplate, setFilters, setDataSeria };

  const clear = useCallback<Filters.Methods.Clear>(() => {
    filters.forEach((f) => f.clear());
  }, [filters]);

  const filter = useCallback<Filters.Methods.Filter<TItem>>(
    <T extends Partial<TItem>>(list: T[], doNotCopyUnfiltered?: boolean) => {
      filters.forEach((f) => (f as FilterInternal).setCount(0));

      const activeFilters = filters.filter((f) => f.active);

      return !activeFilters.length
        ? doNotCopyUnfiltered
          ? list
          : [...list]
        : filterNested(activeFilters, nested, list);
    },
    [dataSeria]
  );

  const useText = UseFilterText(props);
  const useDate = UseFilterDate(props);
  const useNumber = UseFilterNumber(props);
  const useCurrency = UseFilterCurrency(props);
  const useFlags = UseFilterFlagsSet(props);
  const useSet = UseFilterArraySet(props);
  const useBool = UseFilterBool(props);

  const useCustom = UseFilterCustom(props);

  return {
    filters,
    dataSeria,
    clear,
    filter,
    useText,
    useDate,
    useNumber,
    useCurrency,
    useFlags,
    useSet,
    useBool,
    useCustom,
  };
}

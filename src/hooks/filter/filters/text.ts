import { useCallback, useMemo, useState } from 'react';
import _ from 'lodash';

import { Filter, Filters } from '@types';

import { FiltersProps } from '../types';
import { useFilter } from './base';

export function UseFilterText<TItem>(
  listProps: FiltersProps<TItem>
): Filters.Methods.UseText<TItem> {
  return (field) => {
    const [value, setValue] = useState('');
    const [mode, setMode] = useState<Filter.Text.Mode>(Filter.Text.Mode.simple);

    const { regExp, error } = useMemo(() => {
      let regExp: RegExp;
      let error: Error;
      if (value)
        try {
          regExp = new RegExp(
            Filter.Text.Mode.simple === mode ? _.escapeRegExp(value) : value,
            'i'
          );
        } catch (e) {
          regExp = undefined;
          error = e;
        }
      return { regExp, error };
    }, [value, mode]);

    return useFilter<TItem, string, Filter.Text.Extender, Filter.Text<TItem>>(
      listProps,
      Filter.Type.text,
      field,
      !!regExp,
      useCallback(() => setValue(''), []),
      useCallback(
        (value: any): boolean => (regExp ? regExp.test(value) : true),
        [regExp]
      ),
      {
        value,
        setValue,
        mode,
        setMode,
        error,
      }
    );
  };
}

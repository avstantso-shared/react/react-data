import { Filters } from '@types';

import { FiltersProps } from '../types';
import { getFieldValue } from '../utils';

import { UseFilterText } from './text';

export function UseFilterNumber<TItem>(
  listProps: FiltersProps<TItem>
): Filters.Methods.UseNumber<TItem> {
  const useFilterText = UseFilterText(listProps);

  return (field) =>
    useFilterText((item) => {
      const value = getFieldValue(item, field);

      return value.toString();
    });
}

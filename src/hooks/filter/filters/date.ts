import moment from 'moment';

import { Filters } from '@types';

import { FiltersProps } from '../types';
import { getFieldValue } from '../utils';

import { UseFilterText } from './text';

export function UseFilterDate<TItem>(
  listProps: FiltersProps<TItem>
): Filters.Methods.UseDate<TItem> {
  const useFilterText = UseFilterText(listProps);

  return (field, format?) =>
    useFilterText((item) => {
      const value = getFieldValue(item, field);

      return moment(value).format(format || 'DD.MM.yyyy');
    });
}

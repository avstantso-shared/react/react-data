import { Filters } from '@types';

import { FiltersProps } from '../types';
import { getFieldValue } from '../utils';

import { UseFilterText } from './text';

export function UseFilterCurrency<TItem>(
  listProps: FiltersProps<TItem>
): Filters.Methods.UseCurrency<TItem> {
  const useFilterText = UseFilterText(listProps);

  return (field) =>
    useFilterText((item) => {
      const value = getFieldValue(item, field);

      return value.toString();
    });
}

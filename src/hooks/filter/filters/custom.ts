import { useCallback, useMemo, useState } from 'react';
import _ from 'lodash';

import { Filter, Filters } from '@types';

import { FiltersProps } from '../types';
import { useFilter } from './base';

export function UseFilterCustom<TItem>(
  listProps: FiltersProps<TItem>
): Filters.Methods.UseCustom<TItem> {
  return <TValue = any, TExtender extends object = {}>(
    field: Filter.Filed<TItem, TValue>,
    extender: Filter.Custom.Extender & TExtender
  ) => {
    const { active, clear, filter, ...rest } = extender;

    return useFilter<
      TItem,
      TValue,
      typeof rest,
      Filter.Custom<TItem, TValue, TExtender>
    >(listProps, Filter.Type.custom, field, active, clear, filter, rest);
  };
}

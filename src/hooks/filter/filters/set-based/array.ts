import { useCallback, useMemo } from 'react';

import { Generics, Perform } from '@avstantso/node-or-browser-js--utils';

import { PerformCaptions, Filter, Filters } from '@types';

import { FiltersProps } from '../../types';
import { useFilterBaseSet } from './base';

export function UseFilterArraySet<TItem = any>(
  listProps: FiltersProps<TItem>
): Filters.Methods.UseSet<TItem> {
  function useFilterArraySet<
    TValue extends TSourceItem | TSourceItem[],
    TSourceItem
  >(
    field: Filter.Filed<TItem, TValue>,
    source: Perform<ReadonlyArray<TSourceItem>>,
    options?: Filter.Set.Array.Options<TSourceItem>
  ): Filter.Set<TItem, TValue> {
    const lSource = useMemo<ReadonlyArray<TSourceItem>>(
      () => Perform(source),
      [source]
    );

    const getCaptions: PerformCaptions =
      options?.captions ||
      // source is string array case
      Generics.Cast.To(source);

    const filter = useCallback(
      (values: Set<number>) => {
        const matcher = options?.matcher || ((a, b) => a === b);

        const filterItem = (item: TSourceItem) => {
          const i = lSource.findIndex((sourceItem) =>
            matcher(sourceItem, item)
          );
          return values.has(i);
        };

        return (value: TValue) =>
          Array.isArray(value)
            ? (value as TSourceItem[]).some(filterItem)
            : filterItem(value as TSourceItem);
      },
      [lSource]
    );

    return useFilterBaseSet<TItem, TValue, Filter.Set<TItem, TValue>>(
      listProps,
      field,
      getCaptions,
      filter
    );
  }

  // Filters.Methods.UseSet TValue mismatch, used Cast
  return Generics.Cast.To(useFilterArraySet);
}

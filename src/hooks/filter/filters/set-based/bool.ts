import { useCallback, useMemo } from 'react';

import { Perform } from '@avstantso/node-or-browser-js--utils';

import { PerformCaptions, Filter, Filters } from '@types';

import { FiltersProps } from '../../types';
import { useFilterBaseSet } from './base';

export function UseFilterBool<TItem>(
  listProps: FiltersProps<TItem>
): Filters.Methods.UseBool {
  return (
    field: Filter.Filed<TItem, boolean>,
    captions: Filter.Bool.Captions
  ): Filter.Bool<TItem> => {
    const getCaptions: PerformCaptions = useMemo(
      () => () => [Perform(captions.false), Perform(captions.true)],
      [captions.false, captions.true]
    );

    const filter = useCallback(
      (values: Set<number>) => (value: boolean) =>
        (!value && values.has(0)) || (!!value && values.has(1)),
      []
    );

    return useFilterBaseSet<TItem, boolean, Filter.Bool<TItem>>(
      listProps,
      field,
      getCaptions,
      filter,
      Filter.Type.bool
    );
  };
}

import { useCallback } from 'react';

import { PerformCaptions, Filter, Filters } from '@types';

import { FiltersProps } from '../../types';
import { useFilterBaseSet } from './base';

export function UseFilterFlagsSet<TItem = any>(
  listProps: FiltersProps<TItem>
): Filters.Methods.UseFlags<TItem> {
  return <TValue extends number>(
    field: Filter.Filed<TItem, TValue>,
    captions: PerformCaptions
  ): Filter.Set<TItem, TValue> => {
    const filter = useCallback(
      (values: Set<number>) => (value: TValue) => {
        const vs = Array.from(values);
        for (let i = 0; i < vs.length; i++)
          if ((1 << vs[i]) & value) return true;

        return false;
      },
      []
    );

    return useFilterBaseSet<TItem, TValue, Filter.Set<TItem, TValue>>(
      listProps,
      field,
      captions,
      filter
    );
  };
}

import { useCallback, useMemo, useState } from 'react';

import { Perform } from '@avstantso/node-or-browser-js--utils';

import { Caption, PerformCaptions, Filter } from '@types';

import { FiltersProps } from '../../types';
import { useFilter } from '../base';

export function useFilterBaseSet<
  TItem,
  TValue,
  TFilter extends Filter.Set<TItem, TValue> | Filter.Bool<TItem>
>(
  listProps: FiltersProps<TItem>,
  field: Filter.Filed<TItem, TValue>,
  captions: PerformCaptions,
  filter: (values: Set<number>) => (value: TValue) => boolean,
  type: Filter.Type = Filter.Type.set
): TFilter {
  const [values, setValues] = useState(new Set<number>());

  const lCaptions = useMemo<ReadonlyArray<Caption>>(
    () => Perform(captions),
    [captions]
  );

  return useFilter<TItem, TValue, Filter.Set.Extender, TFilter>(
    listProps,
    type,
    field,
    !!values.size,
    useCallback(() => setValues(new Set<number>()), []),
    useMemo(() => filter(values), [values, filter]),
    {
      captions: lCaptions,
      values,
      setValues,
    }
  );
}

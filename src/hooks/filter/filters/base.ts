import { useEffect, useRef, useState } from 'react';

import { Generics } from '@avstantso/node-or-browser-js--utils';

import { Filter } from '@types';
import { ReassignmentError } from '@classes';
import { useIndexedChild } from '@hooks/indexedChild';

import { FiltersProps } from '../types';

export function useFilter<
  TItem,
  TValue,
  TExtender extends
    | Filter.Text.Extender
    | Filter.Set.Extender
    | Omit<Filter.Custom.Extender, 'active' | 'clear' | 'filter'>,
  TFilter extends
    | Filter.Text<TItem>
    | Filter.Set<TItem, TValue>
    | Filter.Bool<TItem>
    | Filter.Custom<TItem>
>(
  listProps: FiltersProps<TItem>,
  type: Filter.Type,
  field: Filter.Filed<TItem, TValue>,
  active: boolean,
  clear: () => void,
  filter: (value: TValue) => boolean,
  extender: TExtender
): TFilter {
  const [isOpen, setIsOpen] = useState(false);
  const [count, setCount] = useState(0);
  const indexRef = useRef(-1);

  // Change data filtering
  useEffect(() => {
    listProps.setDataSeria((prev) => prev + 1);
  }, [active, filter]);

  // Change visual
  return useIndexedChild<TFilter>(
    `Filter ${indexRef.current} for ${type}`,
    // FilterList<TItem> / TFilter[] mismatch, used Cast
    Generics.Cast.To(listProps.setFilters),
    indexRef,
    (prev) => {
      if (prev.type && prev.type !== type)
        throw new ReassignmentError('Filter change type attempt');

      // Add ...extender & setCount fields => typecheck failed => Cast
      return Generics.Cast.To({
        index: indexRef.current,
        id: `filter-${listProps.idTemplate}-${indexRef.current}`,
        count,
        setCount,
        type,
        isOpen,
        setIsOpen,
        field,
        active,
        clear,
        filter,
        ...extender,
      });
    },
    [isOpen, count, type, active, clear, filter, ...Object.values(extender)]
  );
}

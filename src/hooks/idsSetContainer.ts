import { useState } from 'react';

import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type { IdsSet } from '@types';

function empty() {
  return new Set<string>();
}

function fromDataList(list: ReadonlyArray<Model.IDed>): IdsSet {
  return new Set<Model.ID>((list || []).map((item) => item.id));
}

export function useIdsSetContainer(): IdsSet.Container {
  const [data, setData] = useState(empty);
  const clone = () => new Set<Model.ID>(data);
  const clear = () => setData(empty);

  return {
    data,
    setData,
    clone,
    clear,
  };
}

export default { empty, fromDataList } as IdsSet.Management;

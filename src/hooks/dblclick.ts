import { useRef } from 'react';
import type { DblClickController } from '@types';

export function useDblClickController(): DblClickController {
  const clickTimer = useRef<NodeJS.Timeout>(null);

  const cancel = () => clearTimeout(clickTimer.current);

  const click = (func: () => any) => {
    cancel();
    clickTimer.current = setTimeout(() => {
      func();
    }, 550);
  };

  const dblClick = (func: () => any) => {
    cancel();
    func();
  };

  return { click, dblClick };
}

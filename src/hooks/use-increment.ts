import React from 'react';

/**
 * @summary `useState(0)` wrapper for notify state change
 */
export function useIncrement(): {
  (): number;
  peek: number;
} {
  const [state, setIncrement] = React.useState(0);

  let r: number = state;

  function increment() {
    setIncrement((prev) => (r = prev + 1));
    return r;
  }

  return Object.defineProperties<any>(increment, {
    peek: {
      get() {
        return r;
      },
    },
  });
}

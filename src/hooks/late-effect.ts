import { DependencyList, useEffect } from 'react';
import { JS, X, Canceler } from '@avstantso/node-or-browser-js--utils';

let defaultTimeout = 200;

export namespace LateEffect {
  export type ActionCallback = (context: Canceler.Context) => unknown;
  export type CancelCallback = (isActionInvoked?: boolean) => unknown;

  export type Result = {
    /**
     * @summary Cancel effect action
     */
    cancel(): void;
  };

  export namespace Use {
    export type Overload = {
      /**
       * @summary Effect: call `actionCallback` after timeout if not canceled every render if `deps` changed
       * @param timeout Timeout in milliseconds to be waited before actionCallback
       * @param actionCallback Effect action callback
       * @param cancelCallback Callback for effect cancelation
       * @param deps Effect deps
       */
      (
        timeout: number,
        actionCallback: ActionCallback,
        cancelCallback?: CancelCallback,
        deps?: DependencyList
      ): LateEffect.Result;

      /**
       * @summary Effect: call `actionCallback` after timeout if not canceled every render if `deps` changed
       * @param timeout Timeout in milliseconds to be waited before actionCallback
       * @param actionCallback Effect action callback
       * @param deps Effect deps
       */
      (
        timeout: number,
        actionCallback: ActionCallback,
        deps?: DependencyList
      ): LateEffect.Result;

      /**
       * @summary Effect: call `actionCallback` after default timeout if not canceled every render if `deps` changed
       * @param actionCallback Effect action callback
       * @param cancelCallback Callback for effect cancelation
       * @param deps Effect deps
       */
      (
        actionCallback: ActionCallback,
        cancelCallback?: CancelCallback,
        deps?: DependencyList
      ): LateEffect.Result;

      /**
       * @summary Effect: call `actionCallback` after default timeout if not canceled every render if `deps` changed
       * * @param actionCallback Effect action callback
       * @param deps Effect deps
       */
      (
        actionCallback: ActionCallback,
        deps?: DependencyList
      ): LateEffect.Result;
    };
  }

  /**
   * @summary Effect: call `actionCallback` after timeout if not canceled every render if `deps` changed
   * @param timeout Timeout in milliseconds to be waited before actionCallback
   * @param actionCallback Effect action callback
   * @param cancelCallback Callback for effect cancelation
   * @param deps Effect deps
   */
  export type Use = Use.Overload & {
    /**
     * @summary `get/set` default timeout value in milliseconds
     */
    defaultTimeout: number;
  };
}

const useLateEffectRaw: LateEffect.Use.Overload = (
  ...params: any[]
): LateEffect.Result => {
  const hasTimeout = JS.is.function(params[0]) ? 0 : 1;
  const hasCancel = Array.isArray(params[1 + hasTimeout]) ? 0 : 1;

  const deps: DependencyList = params[hasTimeout + hasCancel + 1];

  let cancel: LateEffect.Result['cancel'] = X;

  useEffect(() => {
    const timeout = hasTimeout ? params[0] : defaultTimeout;
    const actionCallback: LateEffect.ActionCallback = params[hasTimeout];
    const cancelCallback: LateEffect.CancelCallback =
      hasCancel && params[hasTimeout + 1];

    const canceler = Canceler();
    let timeoutId = setTimeout(() => {
      actionCallback(canceler.context);
      timeoutId = null;
    }, timeout);

    cancel = () => {
      if (canceler.isCanceled) return;

      canceler.cancel();

      timeoutId && clearTimeout(timeoutId);

      cancelCallback && cancelCallback(!timeoutId);
    };

    return cancel;
  }, deps);

  return {
    cancel,
  };
};

/**
 * @summary Effect: call `actionCallback` after timeout if not canceled every render if `deps` changed
 * @param timeout Timeout in milliseconds to be waited before actionCallback
 * @param actionCallback Effect action callback
 * @param cancelCallback Callback for effect cancelation
 * @param deps Effect deps
 */
export const useLateEffect = Object.defineProperties(
  useLateEffectRaw as LateEffect.Use,
  {
    defaultTimeout: {
      get(): number {
        return defaultTimeout;
      },
      set(value: number) {
        defaultTimeout = value;
      },
    },
  }
);

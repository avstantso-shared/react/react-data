import { useEffect } from 'react';

import { JS, Perform } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import type { Folding } from '@types';

import IdsSetManagement, { useIdsSetContainer } from '@hooks/idsSetContainer';

const { empty, fromDataList } = IdsSetManagement;

export function useFolding<T extends Model.IDed>(): Folding<T> {
  const { data, setData, clone, clear } = useIdsSetContainer();

  useEffect(() => {
    setData(empty());
    return () => {
      setData(empty());
    };
  }, []);

  const toggleFor: Folding.ToggleForOverload<T> = (...params: any[]) => {
    if (!params.length) return;

    const isFODef = JS.is.boolean(params[0]);
    const isFoldedOut: boolean = isFODef && params[0];
    const items: (T | Model.IDed | Model.ID)[] = isFODef
      ? params.splice(1, params.length - 1)
      : params;

    setData((prevState: Set<Model.ID>) => {
      const nextState = new Set<Model.ID>(prevState);

      items
        .map((item) => (JS.is.string(item) ? item : item.id))
        .forEach((id) =>
          isFODef
            ? isFoldedOut
              ? nextState.add(id)
              : nextState.delete(id)
            : nextState.has(id)
            ? nextState.add(id)
            : nextState.delete(id)
        );

      return nextState;
    });
  };

  const toggleAll: Folding.ToggleAllOverload<T> = (...params: any[]) => {
    const isFODef = JS.is.boolean(params[0]);
    const isFoldedOut: boolean = isFODef && params[0];
    const allItems: Perform<ReadonlyArray<T>> = params[isFODef ? 1 : 0];

    if (isFODef)
      return setData(isFoldedOut ? fromDataList(Perform(allItems)) : empty());

    setData((prevState: Set<Model.ID>) => {
      const nextState = new Set<Model.ID>(prevState);

      Perform(allItems)
        .map((item) => (JS.is.string(item) ? item : item.id))
        .forEach((id) =>
          !nextState.has(id) ? nextState.add(id) : nextState.delete(id)
        );

      return nextState;
    });
  };

  const entityGetFolding = (entity: T) => data.has(entity.id);
  const entitySetFolding = (entity: T, isFoldedOut: boolean) => {
    setData((prevState: Set<Model.ID>) => {
      const nextState = new Set<Model.ID>(prevState);
      isFoldedOut ? nextState.add(entity.id) : nextState.delete(entity.id);
      return nextState;
    });
  };

  const filter = (allItems: Perform<ReadonlyArray<T>>) =>
    Perform(allItems).filter(({ id }) => data.has(id));

  return {
    data,
    setData,
    clone,
    clear,
    toggleAll,
    toggleFor,
    entityGetFolding,
    entitySetFolding,
    filter,
  };
}

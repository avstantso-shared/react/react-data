import React, { useEffect, useRef } from 'react';

import { Generics, Perform } from '@avstantso/node-or-browser-js--utils';

import { Indexed } from '@types';
import useDebugSentry from '@hooks/debugSentry';

export function sameIndexRef(
  a: React.MutableRefObject<number> | Indexed | Indexed.Ref,
  b: React.MutableRefObject<number> | Indexed | Indexed.Ref
): boolean {
  return (Generics.Cast(a).indexRef || a) === (Generics.Cast(b).indexRef || b);
}

export function findIndexRef<T extends Indexed>(
  a: React.MutableRefObject<number> | Indexed | Indexed.Ref
) {
  return (b: T): boolean => sameIndexRef(a, b);
}

export function useIndexedChild<TChild extends Indexed>(
  label: string,
  setParentList: React.Dispatch<React.SetStateAction<TChild[]>>,
  indexRef: React.MutableRefObject<number>,
  current: Perform<TChild, [TChild]>,
  deps: React.DependencyList
): TChild {
  const childRef = useRef<TChild>(
    Generics.Cast.To({
      indexRef,
    })
  );

  const { update } = useDebugSentry({ label });

  useEffect(() => {
    setParentList((prev) => {
      indexRef.current = prev.length;
      return [...prev, childRef.current];
    });
  }, []);

  useEffect(() => {
    update(deps);

    setParentList((prev) => {
      const child: TChild = { indexRef, ...Perform(current, childRef.current) };
      childRef.current = child;

      const next = [...prev];
      // AVStantso: parent list order may be changed, if it possible by derived component design
      const realIndex = next.findIndex(findIndexRef(indexRef));
      next.splice(realIndex, 1, child);

      return next;
    });
  }, deps);

  return childRef.current;
}

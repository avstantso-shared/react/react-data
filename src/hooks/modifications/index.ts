import { Modifications as Types } from '@root/types/modifications';
import {
  sameModifications as same,
  calcModificationState as calcState,
  isModification as is,
} from './utils';

export * from './hooks';

export namespace Modifications {
  export namespace State {
    export type Item<T = any> = Types.Details.Item<T>;
  }

  export type State = Types.Details;

  export type Check<T = any> = Types.Check<T>;
  export type Checks = Types.Checks;

  export type Callback = Types.Callback;

  export namespace Behavior {
    export type Options<TEntity = any> = Types.Behavior.Options<TEntity>;
  }

  export type Options<TEntity = any> = Types.Options<TEntity>;
}

export type Modifications<TEntity = any> = Types<TEntity>;

export const Modifications = { same, calcState, is };

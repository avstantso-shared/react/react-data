import { Buffer } from 'buffer';

import { JS, ValueWrap } from '@avstantso/node-or-browser-js--utils';

import type { Modifications } from '@root/types/modifications';

function isEmpty(v: any) {
  return undefined === v || null === v || '' === v;
}

/**
 * @summary Matching values to calculate whether `c`urrent value and `u`nmodified value have changed
 * `key` is passed for debugging
 */
export function sameModifications(key: string): Modifications.Check {
  return (c: any, u: any): boolean => {
    const t = { c: typeof c, u: typeof u };

    const doCompare = () => {
      const co = 'object' === t.c;
      const uo = 'object' === t.u;

      if (co || uo) {
        if (c === u) return true;

        const ca = Array.isArray(c);
        const ua = Array.isArray(u);
        if (ca && ua) {
          if (c.length !== u.length) return false;

          for (let i = 0; i < c.length; i++)
            if (!sameModifications(`${key}[${i}]`)(c[i], u[i])) return false;
        } else if (ca || ua) return false;
        else if (co && uo) {
          if (
            [c, u].some(
              (x) =>
                x instanceof Buffer ||
                x instanceof Date ||
                x instanceof ValueWrap
            )
          )
            return c.valueOf() === u.valueOf();

          if ((co && isEmpty(u)) || (uo && isEmpty(c)))
            return (c?.id || '') === (u?.id || '');
        }
      }
      // same type -> same value
      else if (t.c === t.u) return c === u;
      // '' == undefined
      else if ('undefined' === t.u) return !c ? true : false;
      // '' == 0, 'n' == n
      else if ('string' === t.c && 'number' === t.u)
        return '' === c ? 0 === u : `${u}` === c;

      return false;
    };

    const r = doCompare();

    // console.log(`${key}:`, { t, c, u, r });

    return r;
  };
}

/**
 * @summary Entity mapping to calculate if there are changes between currentEntity and unmodifiedEntity
 * Calls the sameModifications function for each pair of fields up to the first difference
 * or custom check if it is in customChecks
 * When debug checks all fields, even if the result is already clear
 */
export function calcModificationState<TEntity = any>(
  currentEntity: TEntity,
  unmodifiedEntity: TEntity,
  customChecks?: Modifications.Checks,
  debug?: boolean
): Modifications.State {
  const state: Modifications.Details = {};

  const _isSame = (key: string): boolean => {
    const current = currentEntity[key as keyof object];
    const unmodified = (unmodifiedEntity || {})[key as keyof object];

    const checker: Modifications.Check =
      (customChecks && customChecks[key as keyof object]) ||
      sameModifications(key);
    const isMatch = checker(current, unmodified);
    const stateItem: Modifications.Details.Item = {
      current,
      unmodified,
      isMatch,
    };
    state[key] = stateItem;
    return isMatch;
  };

  const fields = Object.keys(currentEntity);
  const modified = !debug
    ? !fields.every((key) => _isSame(key))
    : !fields.reduce((r, key) => _isSame(key) && r, true);

  return { modified, details: state };
}

export function isModification(candidate: unknown): candidate is Modifications {
  return (
    null !== candidate &&
    JS.is.object(candidate) &&
    'modified' in candidate &&
    'setModified' in candidate &&
    'state' in candidate &&
    'prompt' in candidate &&
    'update' in candidate &&
    'save' in candidate &&
    'confirm' in candidate
  );
}

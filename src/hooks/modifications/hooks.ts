import { useEffect, useMemo } from 'react';

import { Generics, JS, Perform } from '@avstantso/node-or-browser-js--utils';

import { T } from '@i18n-utils';
import { useTranslation, LOCALES } from '@i18n';
import type { Modifications } from '@root/types/modifications';

import { useIncrement } from '@hooks/use-increment';
import { useInitRef } from '@hooks/use-init-ref';
import { Reducer } from '@hooks/reducer';

import { calcModificationState } from './utils';

type ModificationsRef<TEntity> = {
  state: Modifications.State;
  dispatch: any;

  unmodified: TEntity;
  t: T;
} & Pick<
  Modifications.Raw<TEntity>,
  'setModified' | 'update' | 'save' | 'confirm'
>;

const { useRefReducer, meta, unsupported } = Reducer<Modifications.State>()
  .as((f) => ({
    SET_MODIFIED: f('modified'),
    SET_DETAILS: f('details'),
  }))
  .common((state, message) => {
    if ('SET_ALL' === String(message.msg)) {
      return Generics.Cast.To(message.payload);
    }

    unsupported(message);
  });

function initializeMethods<TEntity>(
  current: ModificationsRef<TEntity>,
  options: Modifications.Options<TEntity>
): void {
  type MR = ModificationsRef<TEntity>;

  const { silent, customChecks, extCheck, debug } = options;

  const dispatch = current.dispatch as typeof meta.dispatch;

  const setModified: MR['setModified'] = dispatch.SET_MODIFIED;

  const update: MR['update'] = (entity) => {
    const { unmodified } = current;

    const newState = calcModificationState(
      entity,
      unmodified,
      customChecks,
      debug
    );

    newState.modified = !!(newState.modified || (extCheck && extCheck(entity)));

    dispatch(Generics.Cast({ msg: 'SET_ALL', payload: newState }));
  };

  const save: MR['save'] = <TParams extends any[] = never, TResult = unknown>(
    callback: Modifications.Callback<TParams, TResult>,
    ...params: TParams
  ): TResult => {
    const {
      state: { modified },
    } = current;

    if (!modified) return;

    setModified((prev) => {
      // If the callback explicitly returned false, then there was no save
      const r = callback(...params);
      return JS.is.boolean(r) && !r ? prev : false;
    });
  };

  const _confirm: MR['confirm'] = <
    TParams extends any[] = never,
    TResult = unknown
  >(
    callback: Modifications.Callback<TParams, TResult>,
    ...params: TParams
  ): TResult => {
    const {
      state: { modified },
      t,
    } = current;

    if (silent || !modified || confirm(t(LOCALES.hasModificationsMsg)))
      return callback(...params);
  };

  JS.patch(current, {
    setModified,
    update,
    save,
    confirm: _confirm,
  });
}

function fromRef<TEntity>(
  current: ModificationsRef<TEntity>,
  options: Modifications.Options<TEntity>
): Modifications<TEntity> {
  const {
    // -local fields
    state,
    dispatch,
    unmodified,
    t,

    ...rest
  } = current;

  return {
    ...rest,
    get modified() {
      return current.state.modified;
    },
    get details() {
      return current.state.details;
    },
    get prompt() {
      return {
        when: !options?.silent && current.state.modified,
        message: current.t(LOCALES.hasModificationsMsg),
      };
    },
  };
}

/**
 * @summary Controlling entity modifications
 */
export function useModifications<TEntity extends Perform.Able = any>(
  unmodifiedEntity: Perform<TEntity>,
  options: Modifications.Options<TEntity> = {}
): Modifications<TEntity> {
  const [ref, initial] = useInitRef<ModificationsRef<TEntity>>(() => ({
    state: { modified: !!options.defaultState, details: {} },
    unmodified: Perform(unmodifiedEntity),
  }));

  const [state] = useRefReducer.Raw(ref, initial, useIncrement());
  const [t, i18n] = useTranslation();

  ref.current.t = t;

  if (initial) initializeMethods(ref.current, options);

  useEffect(() => {
    ref.current.unmodified = Perform(unmodifiedEntity);
  }, [unmodifiedEntity]);

  // Confirm closing a tab or window.
  // In Firefox, Chrome, Opera, a dialog with a default message is shown.
  // https://js.plainenglish.io/how-to-alert-a-user-before-leaving-a-page-in-react-a2858104ca94
  // https://inthetechpit.com/2020/05/19/prompt-user-with-beforeunload-on-browser-tab-close-react-app/
  useEffect(() => {
    if (options?.silent || !state.modified) return;

    const handleUnload = (e: Event) => {
      e.preventDefault();

      const msg = t(LOCALES.hasModificationsMsg);

      Generics.Cast(e || window.event).returnValue = msg; // Gecko + IE
      return msg;
    };

    window.addEventListener('beforeunload', handleUnload);
    return () => {
      window.removeEventListener('beforeunload', handleUnload);
    };
  }, [state.modified, i18n.language]);

  return useMemo<Modifications<TEntity>>(
    () => fromRef(ref.current, options),
    [state]
  );
}

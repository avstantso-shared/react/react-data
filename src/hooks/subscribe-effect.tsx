import { DependencyList, useLayoutEffect } from 'react';

export type SubscribeEffect = {
  /**
   * @summary `useLayoutEffect` with `element.addEventListener` on mount and `element.removeEventListener` on unmount
   * @param element Element for subscribe
   * @param type Event type
   * @param listener Event hook
   * @param options Event options
   * @param deps  Effect deps
   */
  <K extends keyof HTMLElementEventMap>(
    element: HTMLElement,
    type: K,
    listener: (this: HTMLElement, ev: HTMLElementEventMap[K]) => any,
    options?: boolean | AddEventListenerOptions,
    deps?: DependencyList
  ): void;

  /**
   * @summary `useLayoutEffect` with `element.addEventListener` on mount and `element.removeEventListener` on unmount
   * @param element Element for subscribe
   * @param type Event type
   * @param listener Event hook
   * @param deps  Effect deps
   */
  <K extends keyof HTMLElementEventMap>(
    element: HTMLElement,
    type: K,
    listener: (this: HTMLElement, ev: HTMLElementEventMap[K]) => any,
    deps?: DependencyList
  ): void;

  /**
   * @summary `useLayoutEffect` with `element.addEventListener` on mount and `element.removeEventListener` on unmount
   * @param element Element for subscribe
   * @param type Event type
   * @param listener Event hook
   * @param options Event options
   * @param deps  Effect deps
   */
  (
    element: HTMLElement,
    type: string,
    listener: EventListenerOrEventListenerObject,
    options?: boolean | AddEventListenerOptions,
    deps?: DependencyList
  ): void;

  /**
   * @summary `useLayoutEffect` with `element.addEventListener` on mount and `element.removeEventListener` on unmount
   * @param element Element for subscribe
   * @param type Event type
   * @param listener Event hook
   * @param deps  Effect deps
   */
  (
    element: HTMLElement,
    type: string,
    listener: EventListenerOrEventListenerObject,
    deps?: DependencyList
  ): void;
};

/**
 * @summary `useLayoutEffect` with `element.addEventListener` on mount and `element.removeEventListener` on unmount
 * @param element Element for subscribe
 * @param type Event type
 * @param listener Event hook
 * @param options Event options
 * @param deps  Effect deps
 */
export const useSubscribeEffect: SubscribeEffect = (
  element: HTMLElement,
  type: string,
  listener: EventListenerOrEventListenerObject,
  ...rest: any[]
) => {
  const options: boolean | AddEventListenerOptions =
    !Array.isArray(rest[0]) && rest[0];
  const deps: DependencyList = Array.isArray(rest[0]) ? rest[0] : rest[1];

  useLayoutEffect(() => {
    element.addEventListener(type, listener, options);
    return () => {
      element.removeEventListener(type, listener, options);
    };
  }, deps);
};

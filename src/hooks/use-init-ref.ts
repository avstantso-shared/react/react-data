import React from 'react';

/**
 * @summary `React.useRef` with initalization funtion
 * @param initializer Optionally initializer. If empty — will initialized as `{}`
 * @returns Array: `ref`, `initial` attribute
 */
export function useInitRef<T extends object>(
  initializer?: () => Partial<T>
): [React.MutableRefObject<T>, boolean] {
  const ref = React.useRef<T>();
  const initial = !ref.current;

  if (initial) ref.current = (initializer ? initializer() : {}) as T;

  return [ref, initial];
}

/* eslint no-bitwise: "off" */
import React, { useEffect, useRef, useMemo } from 'react';

import { JS, Perform, X } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import dbg from '@debug';
import { DataCollection } from '@types';

import {
  prohibitDirectModification,
  triggersCall,
  validateCollectionUnique,
} from './utils';

import { useIncrement } from '@hooks/use-increment';
import { useInitRef } from '@hooks/use-init-ref';
import { Reducer } from '@hooks/reducer';

const initialState: DataCollection.State<any> = {
  list: [],
  total: undefined,
};

const { useRefReducer, meta } = Reducer(initialState).as((f) => ({
  SET_LIST: f('list'),
  SET_TOTAL: f('total'),
}));

type DataCollectionHack<T extends Model.Select> = DataCollection<T> & {
  triggers: DataCollection.TriggersList<T>;
};

type DataCollectionRef<T extends Model.Select> = {
  state: DataCollection.State<T>;
  dispatch: any;

  prevList: ReadonlyArray<T>;
  triggers: DataCollection.TriggersList<T>;
} & Pick<DataCollection.Readable<T>, 'version' | 'copy'> &
  DataCollection.Writable<T>;

function initializeMethods<T extends Model.Select>(
  current: DataCollectionRef<T>,
  options: DataCollection.Options<T>
): void {
  type DCR = DataCollectionRef<T>;
  const { label, unique, desc } = options;

  const dispatch = current.dispatch as typeof meta.dispatch;

  // AVStantso:
  // The data in "list" is always sorted by "putdate",
  // old data before, new data later.
  // Sorting makes triggers work.
  //
  // Sorting by "id" doesn't work for GUIDs
  const compare = Perform(desc)
    ? Model.PutDate.Compare.desc
    : Model.PutDate.Compare;

  const validate = Perform(unique) ? validateCollectionUnique : X;

  const setter: DCR['setter'] = (param) =>
    dispatch.SET_LIST((prev) => {
      current.prevList = prev;

      const newList: T[] = [...(Perform(param, prev) || [])];
      newList.sort(compare);

      validate(newList, label);

      prohibitDirectModification(newList, label);

      return newList;
    });

  const setTotal: DCR['setTotal'] = dispatch.SET_TOTAL;

  const copy: DCR['copy'] = () => [...current.state.list];

  const clear: DCR['clear'] = () => current.state.list.length && setter([]);

  const replace: DCR['replace'] = (...rest) =>
    current.state.list.length &&
    setter((prev) => {
      const next = [...prev];

      rest.forEach((param) =>
        next.splice((next as Model.IDed[]).by.id.findIndex(param.id), 1, param)
      );

      return next;
    });

  const add: DCR['add'] = (...rest) => setter((prev) => [...prev, ...rest]);

  const addOrSet: DCR['addOrSet'] = (...rest) =>
    setter((prev) => [
      ...prev.filter((item) => !(rest as Model.IDed[]).by.id.some(item.id)),
      ...rest,
    ]);

  const _delete: DCR['delete'] = (...rest) =>
    current.state.list.length &&
    setter((prev) => {
      const next = [...prev];

      rest.forEach((param) =>
        next.splice(
          (next as Model.IDed[]).by.id.findIndex(
            JS.is.string(param) ? param : param.id
          ),
          1
        )
      );

      return next;
    });

  JS.patch(current, {
    // State
    setter,
    setTotal,

    // Readable
    copy,

    // Writable
    clear,
    replace,
    add,
    addOrSet,
    delete: _delete,
  });
}

function useOriginalEffect<T extends Model.Select>(
  current: DataCollectionRef<T>,
  options: DataCollection.Options<T>
): void {
  const { original } = options;

  useEffect(() => {
    current.setter(Perform(original) || []);
  }, [original]);
}

function useNotifyTriggersEffect<T extends Model.Select>(
  current: DataCollectionRef<T>,
  options: DataCollection.Options<T>
): void {
  const { label } = options;

  useEffect(() => {
    const {
      prevList,
      triggers,
      state: { list },
    } = current;

    if (!prevList) return;

    const enabledTriggers: DataCollection.Trigger<T>[] = triggers.length
      ? triggers.reduce((r, { current: trigger }) => {
          if (!trigger.disabled) r.push(trigger);
          return r;
        }, [])
      : [];

    dbg.dataCollection.subNameSpace(Perform(label))(
      `${enabledTriggers.length}/${triggers.length} triggers to call`
    );

    if (enabledTriggers.length) triggersCall(enabledTriggers, prevList, list);

    current.prevList = undefined;
  }, [current.state.list]);
}

function fromRef<T extends Model.Select>(
  current: DataCollectionRef<T>,
  options: DataCollection.Options<T>
): DataCollectionHack<T> {
  const {
    // -local fields
    state,
    dispatch,
    prevList,

    ...rest
  } = current;

  return new Proxy(
    {
      ...rest,
      get length() {
        return current.state.list?.length || 0;
      },
      get list() {
        return current.state.list;
      },
      get total() {
        return current.state.total;
      },
      get label() {
        return Perform(options.label);
      },
    },
    {
      get(target, p) {
        if (p in target) return JS.get.raw(target, p);

        const { list } = target;
        const original = JS.get.raw(list, p);
        if (!original || !JS.is.function(original)) return original;

        return new Proxy(original, {
          apply: (target, thisArg, argumentsList) =>
            target.call
              ? target.call(list, ...argumentsList)
              : target(...argumentsList),
        });
      },
    }
  ) as DataCollectionHack<T>;
}

export const useDataCollection: DataCollection.Use = <T extends Model.Select>(
  options: DataCollection.Options<T> = {}
): DataCollection<T> => {
  const { label, externalDeps = [] } = options;

  const increment = useIncrement();
  const [ref, initial] = useInitRef<DataCollectionRef<T>>(() => ({
    triggers: [],
    get version() {
      return increment.peek;
    },
  }));

  const [state] = useRefReducer.Raw(ref, initial, increment);

  if (initial) initializeMethods(ref.current, options);

  useOriginalEffect(ref.current, options);
  useNotifyTriggersEffect(ref.current, options);

  return useMemo<DataCollectionHack<T>>(
    () => fromRef(ref.current, options),
    [state, ...externalDeps]
  );
};

export const useDataCollectionTrigger: DataCollection.Trigger.Use = <
  T extends Model.Select
>(
  collection: DataCollection<T>,
  callback: DataCollection.Trigger.Callback<T>,
  {
    label,
    actions = DataCollection.Trigger.Action.Any,
    disabled,
    changesMatch,
  }: DataCollection.Trigger.Options<T> = {}
) => {
  const debug = dbg.dataCollection.trigger.subNameSpace(
    `${Perform(label)}-for-${collection.label}`
  );

  const triggerRef = useRef<DataCollection.Trigger<T>>();

  useEffect(() => {
    const { triggers } = collection as DataCollectionHack<T>;

    triggers.push(triggerRef);

    debug(`Mounted`);

    return () => {
      const index = triggers.indexOf(triggerRef);
      if (index >= 0) triggers.splice(index, 1);

      debug(`Unmounted`);
    };
  }, []);

  useEffect(() => {
    triggerRef.current = {
      callback: (props) => {
        callback(props);
        debug(`Called`);
      },
      actions,
      disabled: !!disabled,
      changesMatch,
      label: Perform(label),
    };

    debug(`updated with %O`, triggerRef.current);
  }, [callback, actions, !!disabled, label]);
};

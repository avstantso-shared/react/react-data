import _ from 'lodash';

import { JS, Perform } from '@avstantso/node-or-browser-js--utils';
import type { Model } from '@avstantso/node-or-browser-js--model-core';

import { DataCollection } from '@types';
import { DataCollectionError } from '@classes';

export function validateCollectionUnique<T extends Model.Select>(
  list: ReadonlyArray<T>,
  label: Perform<string>
) {
  for (let i = 0; i < list.length; i++)
    for (let j = 0; j < list.length; j++)
      if (i !== j && list[i].id === list[j].id) {
        const l = Perform(label);
        throw new DataCollectionError(
          `Id ${list[i].id} not unique in data collection${l ? ` "${l}"` : ''}`
        );
      }
}

export function prohibitDirectModification<T extends Model.Select>(
  list: ReadonlyArray<T>,
  label: Perform<string>
) {
  [
    'pop',
    'push',
    'reverse',
    'shift',
    'sort',
    'splice',
    'unshift',
    'fill',
    'copyWithin',
  ].forEach((m) =>
    JS.set.raw(list, m, () => {
      const l = Perform(label);
      throw new DataCollectionError(
        `Direct modification of the internal list of collection${
          l ? ` "${l}"` : ''
        } is prohibited`
      );
    })
  );
}

export function triggersCall<T extends Model.Select>(
  triggers: DataCollection.Trigger<T>[],
  prev: ReadonlyArray<T>,
  next: ReadonlyArray<T>
) {
  const matchChanges = (
    changesMatch: DataCollection.Trigger<T>['changesMatch'],
    oldData: T,
    newData: T
  ) =>
    changesMatch.every(
      (fieldName) => !_.isEqual(oldData[fieldName], newData[fieldName])
    );

  const doCall = (
    action: DataCollection.Trigger.Action,
    oldData: T,
    newData: T
  ) =>
    triggers
      .filter(
        ({ actions, changesMatch }) =>
          actions & action &&
          (DataCollection.Trigger.Action.Update !== action ||
            !changesMatch?.length ||
            matchChanges(changesMatch, oldData, newData))
      )
      .forEach(({ callback }) =>
        callback({ oldData, newData, action, list: next })
      );

  let p = 0;
  let n = 0;
  while (p < prev.length || n < next.length) {
    const pItem = prev[p];
    const nItem = next[n];

    if (pItem && (!nItem || (nItem && nItem.putdate > pItem.putdate))) {
      doCall(DataCollection.Trigger.Action.Delete, pItem, null);
      p++;
      continue;
    }

    if (nItem && (!pItem || (pItem && pItem.putdate > nItem.putdate))) {
      doCall(DataCollection.Trigger.Action.Insert, null, nItem);
      n++;
      continue;
    }

    if (pItem.version !== nItem.version)
      doCall(DataCollection.Trigger.Action.Update, pItem, nItem);

    p++;
    n++;
  }
}

import { useState } from 'react';

import type { Progress } from '@types';

export function useProgress(): Progress {
  const [value, setValue] = useState<number>(null);

  const reset = () => setValue(null);

  const handler = (event: any) => {
    setValue(Math.round((event.loaded / event.total) * 10000) / 100);
  };

  return {
    get value() {
      return value;
    },
    set value(newValue) {
      setValue(newValue);
    },
    reset,
    handler,
  };
}

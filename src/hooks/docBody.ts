import { useEffect, useRef } from 'react';

import type { DocBodyClickTimer } from '@types';

export function useDocBodyClickTimer(
  canHandle: boolean,
  handler?: () => void
): DocBodyClickTimer {
  // AVStantso: useState is not working properly
  const cleared = useRef<boolean>(false);
  const clear = () => {
    cleared.current = true;
  };

  useEffect(() => {
    const docBodyClick = () => {
      if (canHandle && handler && !cleared.current) {
        handler();
      }

      cleared.current = false;
    };

    if (canHandle) document.body.addEventListener('click', docBodyClick);

    return () => {
      document.body.removeEventListener('click', docBodyClick);
    };
  }, [canHandle, handler]);

  return { clear };
}

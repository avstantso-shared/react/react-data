import React, { useEffect, useRef } from 'react';

export function useIsMountedRef(): React.MutableRefObject<boolean> {
  const isMounted = useRef(true);

  useEffect(() => {
    return () => {
      isMounted.current = false;
    };
  });

  return isMounted;
}

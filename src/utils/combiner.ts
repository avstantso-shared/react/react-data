import React, { CSSProperties } from 'react';

import { Generics, JS } from '@avstantso/node-or-browser-js--utils';

export namespace Combiner {
  export type ClassName = Pick<React.HTMLProps<HTMLElement>, 'className'>;
  export type Style = Pick<React.HTMLProps<HTMLElement>, 'style'>;
  export type Deps = { deps?: React.DependencyList };

  export type Support<
    TProps extends {},
    TAddition extends {},
    TAdditionReplace extends {} = TAddition
  > = Partial<TProps> extends TAddition ? TAdditionReplace : {};

  export type Supported<TProps extends {}> = Support<
    TProps,
    ClassName,
    { className?: string | string[] }
  > &
    Support<TProps, Style, { style?: CSSProperties | CSSProperties[] }> &
    Support<TProps, Deps>;

  export namespace Supported {
    export type All = ClassName & Style & Deps;
  }
}

export type Combiner = {
  /**
   * @summary Combine classNames and/or styles and/or deps: React.DependencyList with React props
   */
  <TProps extends {}>(
    props: TProps,
    addition: Combiner.Supported<TProps>
  ): TProps;

  /**
   * @summary Combine classNames for React props destructure
   */
  className(...classNames: string[]): Combiner.ClassName;

  /**
   * @summary Combine styles for React props destructure
   */
  style(...styles: React.CSSProperties[]): Combiner.Style;

  /**
   * @summary Combine deps: React.DependencyList for React props destructure
   */
  deps(...depsLists: React.DependencyList[]): Combiner.Deps;
};

function className(...classNames: string[]): Combiner.ClassName {
  const className = classNames
    .filter((item) => (JS.is.string(item) ? item.trim() : item))
    .join(' ');
  return className ? { className } : {};
}

function style(...styles: React.CSSProperties[]): Combiner.Style {
  const noEmpty = styles.filter((item) => item && Object.keys(item).length);
  if (!noEmpty.length) return {};

  let style: React.CSSProperties = {};
  noEmpty.forEach((item) => (style = { ...style, ...item }));

  return { style };
}

function deps(...depsLists: React.DependencyList[]): Combiner.Deps {
  const deps: React.DependencyList = depsLists.reduce<any[]>((r, item) => {
    item && r.push(...item);
    return r;
  }, []);
  return deps.length ? { deps } : {};
}

/**
 * @summary Combine classNames and/or styles and/or deps: React.DependencyList with React props
 */
export const Combiner: Combiner = <TProps extends {}>(
  props: TProps,
  addition: Combiner.Supported<TProps>
): TProps => {
  const { className, style, deps, ...rest }: Combiner.Supported.All & TProps =
    Generics.Cast.To(props);

  const add: any = addition;
  const addClassNames: string[] =
    add.className &&
    (Array.isArray(add.className) ? add.className : [add.className]);
  const addStyles: CSSProperties[] =
    add.style && (Array.isArray(add.style) ? add.style : [add.style]);
  const addDeps: React.DependencyList = add.deps;

  return Generics.Cast.To({
    ...(addClassNames ? Combiner.className(className, ...addClassNames) : {}),
    ...(addStyles ? Combiner.style(style, ...addStyles) : {}),
    ...(addDeps ? Combiner.deps(deps, addDeps) : {}),
    ...rest,
  });
};

Combiner.className = className;
Combiner.style = style;
Combiner.deps = deps;

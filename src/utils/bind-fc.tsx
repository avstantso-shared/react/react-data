import { Generic, JS } from '@avstantso/node-or-browser-js--utils';
import React from 'react';

import { Combiner } from './combiner';
import { DynamicFC } from './dynamic-fc';

/**
 * @summary Create `FunctionComponent` on existing `FunctionComponent` with name `name` and binded props
 * @param name Component name
 * @param FC Component for bind
 * @param defProps Component binded props
 * @returns `FunctionComponent` with name `name`
 */
export function BindFC<
  TDefaultProps extends object,
  TProps extends object,
  TFC extends React.FC<TDefaultProps & TProps> = React.FC<
    TDefaultProps & TProps
  >,
  TName extends string = string
>(
  name: TName,
  FC: TFC,
  defProps: TDefaultProps
): React.FC<TProps> & { [K in keyof TFC]: TFC[K] } {
  const proxy = new Proxy(
    DynamicFC(name, (props: Generic) => {
      const {
        className: defClassName,
        style: defStyle,
        ...defRest
      } = defProps as Generic;

      const { className, style, ...rest } = props;

      const combined = {
        ...defRest,
        ...rest,
        ...Combiner.className(defClassName, className),
        ...Combiner.style(defStyle, style),
      };

      return <FC {...combined} />;
    }),
    {
      get: (target, p) => JS.get.raw(p in target ? target : FC, p),
    }
  );

  return proxy as Generic;
}

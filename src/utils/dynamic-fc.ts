import React from 'react';

/**
 * @summary Create `FunctionComponent` with name `name` from `staticFC`
 * @param name Component name
 * @param staticFC Component implementation
 * @returns `FunctionComponent` with name `name`
 */
export function DynamicFC<
  TProps extends object,
  TFC extends React.FC<TProps> = React.FC<TProps>,
  TName extends string = string
>(name: TName, staticFC: TFC): TFC {
  return new Function(
    'component',
    `return function ${name}(props, context){ return component(props, context); };`
  )(staticFC);
}

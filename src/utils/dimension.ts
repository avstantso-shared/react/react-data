import { CSSProperties } from 'react';

import { JS, TS } from '@avstantso/node-or-browser-js--utils';

import { cssAsEmpty } from './css';

type Width = keyof Pick<CSSProperties, 'width'>;
type Height = keyof Pick<CSSProperties, 'height'>;

export type Dimension = Dimension.Horizontal | Dimension.Vertical;
export namespace Dimension {
  export type Horizontal = 'horizontal';
  export type Vertical = 'vertical';

  export type Provider = {
    /**
     * @summary Dimension
     */
    dimension?: Dimension;
  };

  export namespace Provider {
    export type Default = {
      /**
       * @summary Specify `dimension`
       * @default 'horizontal'
       */
      dimension?: Dimension;
    };
  }

  /**
   * @summary Dimension is vertical. `-1` — invalid dimension
   */
  export type IsVertical = boolean | number;
  export namespace IsVertical {
    export type Provider = {
      /**
       * @summary `dimension !== 'horizontal'`
       */
      isVertical: IsVertical;
    };

    export namespace Provider {
      export type Default = {
        /**
         * @summary `dimension !== 'horizontal'`
         * @default 0
         */
        isVertical: IsVertical;
      };
    }
  }

  /**
   * @summary Opposite dimension
   */
  export type Opposite<D extends Dimension> = D extends Horizontal
    ? Vertical
    : D extends Vertical
    ? Horizontal
    : Dimension;

  export namespace Size {
    export namespace Field {
      export type Values<Opposite extends boolean = false> =
        Opposite extends true ? [Height, Width] : [Width, Height];

      export type ByDimension<
        D extends Dimension | IsVertical,
        Opposite extends boolean = false
      > = D extends false | 0
        ? ByDimension<Horizontal, Opposite>
        : D extends true | 1
        ? ByDimension<Vertical, Opposite>
        : D extends Horizontal
        ? Values<Opposite>[0]
        : D extends Vertical
        ? Values<Opposite>[1]
        : Width | Height;

      export type Prefixed<
        D extends Dimension | IsVertical,
        Opposite extends boolean = false,
        P extends string = undefined
      > = P extends undefined
        ? ByDimension<D, Opposite>
        : `${P}${Capitalize<ByDimension<D, Opposite>>}`;
    }

    export type Field = {
      <
        D extends Dimension,
        O extends boolean = false,
        P extends string = undefined
      >(
        dimension: D,
        prefix?: P,
        opposite?: O
      ): Field.Prefixed<D, O, P>;

      <
        V extends IsVertical,
        O extends boolean = false,
        P extends string = undefined
      >(
        isVertical: V,
        prefix?: P,
        opposite?: O
      ): Field.Prefixed<V, O, P>;
    };

    export type CSS<
      D extends Dimension | IsVertical,
      O extends boolean = false,
      P extends string = undefined
    > = {
      [K in Field.Prefixed<D, O, P>]?: CSSProperties[Extract<
        K,
        keyof CSSProperties
      >];
    };
  }

  export type Size = {
    <
      D extends Dimension,
      O extends boolean = false,
      P extends string = undefined
    >(
      dimension: D,
      size: CSSProperties[Width | Height],
      prefix?: P,
      opposite?: O
    ): Size.CSS<D, O, P>;

    <
      V extends IsVertical,
      O extends boolean = false,
      P extends string = undefined
    >(
      isVertical: V,
      size: CSSProperties[Width | Height],
      prefix?: P,
      opposite?: O
    ): Size.CSS<V, O, P>;

    Field: Size.Field;
  };
}

const Def = {
  dimension: TS.Type.Definer<Dimension>(),
  size: TS.Type.Definer<keyof Pick<CSSProperties, Width | Height>>(),
};

const dimensions = Def.dimension.Arr('horizontal', 'vertical');
const sizes = Def.size.Arr('width', 'height');

const dimensionSizeField: Dimension.Size.Field = (
  d: any,
  p: any,
  f: any
): any => {
  const i = JS.is.string<Dimension>(d)
    ? dimensions.indexOf(d)
    : JS.is.number(d)
    ? d
    : JS.is.boolean(d)
    ? d
      ? 1
      : 0
    : -1;
  if (i < 0) return null;

  let r: any = sizes[(f ? !i : i) ? 1 : 0];

  if (p) r = `${p}${r.toCapitalized()}`;

  return r;
};

const dimensionSize: Dimension.Size = (
  d: any,
  size: any,
  prefix?: any,
  free?: any
): any => {
  const r: any = {};

  if (!cssAsEmpty.includes(size)) {
    r[dimensionSizeField(d, prefix, free)] = size;
  }

  return r;
};

dimensionSize.Field = dimensionSizeField;

export const Dimension = {
  Horizontal: dimensions[0],
  Vertical: dimensions[1],
  Values: dimensions,

  isVertical: (dimension: Dimension | string): Dimension.IsVertical =>
    dimensions.indexOf(dimension as Dimension),

  Size: dimensionSize,
};

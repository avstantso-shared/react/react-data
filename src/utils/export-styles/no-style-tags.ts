// Styles inherited from style sheets will not be rendered for elements with these tag names
export const noStyleTags = [
  'BASE',
  'HEAD',
  'HTML',
  'META',
  'NOFRAME',
  'NOSCRIPT',
  'PARAM',
  'SCRIPT',
  'STYLE',
  'TITLE',
];

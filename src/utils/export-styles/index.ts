import { noStyleTags } from './no-style-tags';
import { tagNames } from './tag-names';

// Mapping between tag names and css default values lookup tables. This allows to exclude default values in the result.
const defaultStylesByTagName: any = {};

// Precompute the lookup tables.
for (var i = 0; i < tagNames.length; i++) {
  if (!noStyleTags.includes(tagNames[i])) {
    defaultStylesByTagName[tagNames[i]] = computeDefaultStyleByTagName(
      tagNames[i]
    );
  }
}

function computeDefaultStyleByTagName(tagName: string) {
  const defaultStyle: any = {};
  const element = document.body.appendChild(document.createElement(tagName));
  const computedStyle: any = getComputedStyle(element);
  for (let i = 0; i < computedStyle.length; i++) {
    defaultStyle[computedStyle[i]] = computedStyle[computedStyle[i]];
  }
  document.body.removeChild(element);
  return defaultStyle;
}

function getDefaultStyleByTagName(tagName: string) {
  tagName = tagName.toUpperCase();
  if (!defaultStylesByTagName[tagName]) {
    defaultStylesByTagName[tagName] = computeDefaultStyleByTagName(tagName);
  }
  return defaultStylesByTagName[tagName];
}

/**
 * @summary Export element styles
 * @see Source https://stackoverflow.com/questions/7754469/export-css-of-dom-elements
 */
export function exportStyles(element: Element) {
  if (element.nodeType !== Node.ELEMENT_NODE) {
    throw new TypeError(
      `The exportStyles method only works on elements, not on ${element.nodeType} nodes`
    );
  }

  if (noStyleTags.includes(element.tagName)) {
    throw new TypeError(
      `The exportStyles method does not work on ${element.tagName} elements.`
    );
  }

  const styles: NodeJS.Dict<string> = {};
  const computedStyle = getComputedStyle(element);
  const defaultStyle = getDefaultStyleByTagName(element.tagName);
  for (let i = 0; i < computedStyle.length; i++) {
    const cssPropName: any = computedStyle[i];
    if (computedStyle[cssPropName] !== defaultStyle[cssPropName]) {
      styles[cssPropName] = computedStyle[cssPropName];
    }
  }

  return styles;
  // const r: string[] = [];
  // for (let i in styles) r.push(`${i}: ${styles[i]};`);

  // return r.join('\r\n');
}

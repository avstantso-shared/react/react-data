import { useEffect, useRef, useState } from 'react';

import {
  Generics,
  Perform,
  JS,
  Stub,
  X,
} from '@avstantso/node-or-browser-js--utils';

export namespace IfDev {
  export type Options<T> = {
    /**
     * @summary Custom global reg callback if needed
     */
    reg?: Function;

    /**
     * @summary Result for production
     * @default T extends Function ? () => null : null
     */
    empty?: T;

    /**
     * @summary Extended condition
     */
    condition?: Perform<boolean>;
  };
}

/**
 * @summary Create empty stub in Prod or passed arg in Dev
 * @param inDev Result for Dev
 * @param options Options
 * @returns `inDev` or `options.empty`
 */
export function IfDev<T>(
  inDev: T,
  {
    reg,
    empty = JS.is.function(inDev) ? Generics.Cast.To(() => null) : null,
    condition = true,
  }: IfDev.Options<T> = {}
): T {
  if (!process.env.DEBUG_INFO_ENABLED || !Perform(condition)) return empty;

  reg && reg();

  return inDev;
}

IfDev.useState = !!process.env.DEBUG_INFO_ENABLED
  ? useState
  : ((() => [undefined, X]) as typeof useState);

IfDev.useChangesReport = Stub(
  !!process.env.DEBUG_INFO_ENABLED &&
    (<TProps extends {}>(label: string | Function, props: TProps) => {
      type Rec = { counter: number; last: any; lastChange: boolean };
      type Recs = Partial<Record<keyof TProps, Rec>>;

      const entries = Object.entries(props);

      const statusRef = useRef<Recs>({});

      useEffect(() => {
        entries.forEach(([k, v]) =>
          JS.set.raw(statusRef.current, k, {
            counter: 0,
            last: v,
            lastChange: false,
          })
        );
      }, []);

      useEffect(() => {
        Object.values(statusRef.current).forEach(
          (v) => ((v as Rec).lastChange = false)
        );
      });

      for (let i = 0; i < entries.length; i++) {
        const [k, v] = entries[i];
        useEffect(() => {
          const c: Rec = JS.get.raw(statusRef.current, k);
          c.lastChange = true;
          c.counter += 1;
          c.last = v;
        }, [v]);
      }

      useEffect(() => {
        const cur = Object.entries(statusRef.current).reduce((r, [k, v]) => {
          const { lastChange, ...rec } = v as Rec;
          lastChange && JS.set.raw(r, k, rec);
          return r;
        }, {});

        Object.keys(cur).length &&
          console.log(
            `${JS.is.function(label) ? label.name : label} changes report: %O`,
            cur
          );
      });
    })
);

IfDev.useMountedReport = Stub(
  !!process.env.DEBUG_INFO_ENABLED &&
    ((label: string | Function, ...params: any[]) => {
      useEffect(() => {
        const name = JS.is.function(label) ? label.name : label;
        const p = params.map(() => `%O`).join(', ');

        console.log(`${name} mounted ${p}`.trimEnd(), ...params);
        return () => {
          console.log(`${name} unmounted ${p}`.trimEnd(), ...params);
        };
      }, []);
    })
);

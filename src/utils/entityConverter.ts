import { JS } from '@avstantso/node-or-browser-js--utils';
import { EntityFormConverter } from '../types';

// Now it is stub
export const cleanEntity = <T = any>(entity: T): T => entity;

export const EntityNulls = (() => {
  const _entries = <T>(entity: T) =>
    entity
      ? Object.entries(entity).filter(([k]) => k.includes('Id') || 'id' === k)
      : [];

  const _to = <T>(entity: T): T => {
    _entries(entity).forEach(([k, v]) => {
      if (null === v || undefined === v) JS.set.raw(entity, k, '');
    });

    return entity;
  };

  const _from = <T>(entity: T): T => {
    _entries(entity).forEach(([k, v]) => {
      if (v === '') JS.set.raw(entity, k, null);
    });

    return cleanEntity<T>(entity);
  };

  const converter = <T>(overrides?: {
    to?: (entity: T) => T;
    from?: (entity: T) => T;
  }): EntityFormConverter<T> => ({
    to: (entity: T): T =>
      overrides?.to ? overrides.to(_to(entity)) : _to(entity),
    from: (entity: T): T =>
      overrides?.from ? overrides.from(_from(entity)) : _from(entity),
  });

  converter.to = _to;
  converter.from = _from;

  return converter;
})();

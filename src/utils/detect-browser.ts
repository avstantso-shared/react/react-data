// Source:
//   https://stackoverflow.com/questions/9847580/how-to-detect-safari-chrome-ie-firefox-and-opera-browsers

// Opera 8.0+
export const isOpera =
  // @ts-ignore
  (!!window.opr && !!opr.addons) ||
  // @ts-ignore
  !!window.opera ||
  navigator.userAgent.indexOf(' OPR/') >= 0;

// Firefox 1.0+
// @ts-ignore
export const isFirefox = typeof InstallTrigger !== 'undefined';

// Safari 3.0+ "[object HTMLElementConstructor]"
export const isSafari =
  // @ts-ignore
  /constructor/i.test(window.HTMLElement) ||
  (function (p) {
    return p.toString() === '[object SafariRemoteNotification]';
  })(
    // @ts-ignore
    !window['safari'] ||
      // @ts-ignore
      (typeof safari !== 'undefined' && window['safari'].pushNotification)
  );

// Internet Explorer 6-11
// @ts-ignore
export const isIE = /*@cc_on!@*/ false || !!document.documentMode;

// Edge 20+
// @ts-ignore
export const isEdge = !isIE && !!window.StyleMedia;

// Chrome 1 - 79
export const isChrome =
  // @ts-ignore
  !!window.chrome;
// A.V.Stantso: don`t work for my Chrome
// && (!!window.chrome.webstore || !!window.chrome.runtime);

// Edge (based on chromium) detection
export const isEdgeChromium =
  isChrome && navigator.userAgent.indexOf('Edg') != -1;

// Blink engine detection
export const isBlink = (isChrome || isOpera) && !!window.CSS;

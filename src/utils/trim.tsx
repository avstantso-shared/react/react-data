import React from 'react';
import { JS } from '@avstantso/node-or-browser-js--utils';

export function trimStartChildren(children: React.ReactNode) {
  return !Array.isArray(children)
    ? JS.is.string(children)
      ? children.trimStart()
      : children
    : children.map((child, index) => {
        const item =
          0 === index && JS.is.string(child) ? child.trimStart() : child;
        return <React.Fragment key={index}>{item}</React.Fragment>;
      });
}
